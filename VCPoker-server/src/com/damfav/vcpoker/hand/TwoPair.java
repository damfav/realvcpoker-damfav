 /* TwoPair.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.hand;

import java.util.List;

/**
 * A class that models the TwoPair hand.
 * 
 * @author francescbautista
 *
 */
public class TwoPair extends OnePair {

	public TwoPair(List<Card> cards) {
		super(cards);
		// TODO Auto-generated constructor stub
	}
	
	public static boolean are(List<Card> cards){
		AbstractHand h = new TwoPair(cards);
		h.sort();
		
		int onePairRank = 0;
		
		for (int i = 0; i < cards.size() - 1; i++){
			
			if (cards.get(i).getRank() == cards.get(i+1).getRank()
					&& cards.get(i).getRank() != onePairRank){		
				
				if (onePairRank == 0){
					onePairRank = cards.get(i).getRank();
				}else{
					return true;
				}		
				
			}
			
		}
		return false;
	}
	
	@Override
	public int getBaseValue() {
		// TODO Auto-generated method stub
		return 2;
	}

}
