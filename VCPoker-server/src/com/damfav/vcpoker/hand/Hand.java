 /* Hand.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.hand;

import java.util.List;

/**
 * A class that models a Hand in poker.
 * @author local
 *
 */
public interface Hand extends Comparable<Hand>{
	
	/**
	 * This is the base value of the Hand. In poker the hands
	 * are ordered by the type of hand, a superior ranked hand
	 * is always better than inferior ranking hand. This method will 
	 * return a value that represents that ranking.
	 * @return int The ranking of the hand.
	 */
	int getBaseValue();
	
	/**
	 * Returns the cards that this Hand has.
	 * @return
	 */
	List<Card> getCards();
	
	/**
	 * Returns the first Card of the highest sequence of sequenceSize
	 * Card
	 * @param sequenceSize
	 * @return Card
	 */
	Card findHighestSequence(int sequenceSize);
	
	
	/**
	 * Returns the Card with highest rank in the Hand
	 */
	Card findHighestCard();
	
	void sort();

	Card findHighestSequence(int sequenceSize, int rankDiff);

}
