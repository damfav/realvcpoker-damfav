 /* HandFacade.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.hand;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class HandFacade {
	
	/**
	 * This comparators orders a list of Hand in ascending order.
	 */
	public static final Comparator<Hand> handComparator = new Comparator<Hand>() {

		@Override
		public int compare(Hand h1, Hand h2) {
			return h1.compareTo(h2);
		}
	};
	
	public Hand findBestHand(List<Hand> hands){
		return Collections.max(hands, handComparator);
	}
	
	/**
	 * Gets the best Hand that this cards are.
	 * @param cards A List of Card
	 * @return A Hand instance of the highest ranked possible with those cards.
	 */
	public static Hand getHand(List<Card> cards){
		Hand h = null;
		if (cards == null || cards.size() == 0){
			return null;
		}
		if (StraightFlush.are(cards)){
			
			h = new StraightFlush(cards);
			
		}else if(FourOfAKind.are(cards)){
			
			h = new FourOfAKind(cards);
			
		}else if(FullHouse.are(cards)){
			
			h = new FullHouse(cards);
			
		}else if(Flush.are(cards)){
			
			h = new Flush(cards);
			
		}else if(Straight.are(cards)){
			
			h = new Straight(cards);
			
		}else if(ThreeOfAKind.are(cards)){
			
			h = new ThreeOfAKind(cards);
			
		}else if(TwoPair.are(cards)){
			
			h = new TwoPair(cards);
			
		}else if(OnePair.are(cards)){
			
			h = new OnePair(cards);
			
		}else{
			
			h = new HighCard(cards);
		}
		
		return h;
			
	}
	
	
	
}
