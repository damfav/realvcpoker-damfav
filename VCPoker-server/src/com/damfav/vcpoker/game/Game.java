/* Game.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.damfav.vcpoker.dao.DAO;
import com.damfav.vcpoker.dao.DAOFactory;
import com.damfav.vcpoker.hand.Card;
import com.damfav.vcpoker.hand.Hand;
import com.damfav.vcpoker.hand.HandFacade;
import com.damfav.vcpoker.message.Message;
import com.damfav.vcpoker.message.MessageFactory;
import com.damfav.vcpoker.message.RaiseMessage;
import com.damfav.vcpoker.player.Player;
import com.damfav.vcpoker.player.PlayerAction;

public class Game {

	/**
	 * Whether the game is started
	 */
	private boolean isStarted;

	/**
	 * This game's croupier
	 */
	private Croupier croupier;

	/**
	 * Array with all the players of this game at their respective positions.
	 */
	private Player[] players;

	/**
	 * Type of currency this game is being played with
	 */
	private String currencyType;

	/**
	 * Player holding the current turn
	 */
	private Player playerCurrentTurn;

	/**
	 * Already shown center-of-the-table cards
	 */
	private List<Card> boardCards;

	/**
	 * Biggest bet this round
	 */
	private double lastBet;

	/**
	 * Pot amounted this game. Updated round by round
	 */
	private double currentTableMoney;

	/**
	 * This game's Chat object
	 */
	private Chat chat;

	/**
	 * Game instance
	 */
	private static Game instance;

	/**
	 * Actions that should be stored as a pokerEvent
	 */
	private static int[] ACTIONS_POKER_EVENT = new int[] {
			MessageFactory.MSG_FOLD, MessageFactory.MSG_ALLIN,
			MessageFactory.MSG_CHECK, MessageFactory.MSG_CALL,
			MessageFactory.MSG_RAISE, };

	/**
	 * The object that will be used to store and retrieve information from the
	 * database.
	 */
	private DAO dao;

	public Game() {
		this.croupier = new Croupier(this);
		this.players = new Player[9];
		boardCards = new ArrayList<Card>();
		this.chat = new Chat();
		this.currentTableMoney = 0;
		dao = DAOFactory.getDefault();
	}

	/**
	 * Creates an instance of Game.
	 * 
	 * @return an instance of Game
	 */
	public static Game getInstance() {
		if (instance == null) {
			instance = new Game();
		}

		return instance;
	}

	/**
	 * Finds the player or players with the best hand.
	 * 
	 * @return ArrayList of player/s with the best hand.
	 */
	public List<Player> getThePlayerWithTheBestHand() {

		ArrayList<Player> winningPlayers = new ArrayList<Player>();
		Hand bestHand = getBestHand();

		for (Player p : getPossibleWinners()) {
			if (p.getHand().compareTo(bestHand) == 0) {
				winningPlayers.add(p);
			}
		}

		return winningPlayers;

	}

	/**
	 * Gets and spares the pot.
	 * 
	 * @return Map<Integer, Double> of players and amounts
	 */
	public Map<Integer, Double> getWinnerPot() {
		Map<Integer, Double> winnerPot = new HashMap<>();
		List<Player> winners = getThePlayerWithTheBestHand();
		double splitPot = currentTableMoney / winners.size();
		for (Player p : winners) {
			winnerPot.put(p.getId(), splitPot);
		}

		return winnerPot;
	}

	/**
	 * Gets the one best hand at the end of the round.
	 * 
	 * @return the best hand of the round.
	 */
	private Hand getBestHand() {

		HandFacade handFacade = new HandFacade();
		List<Hand> hands = new ArrayList<Hand>();

		for (Player p : getPossibleWinners()) {
			List<Card> playerCards = p.getHand().getCards();
			playerCards.addAll(boardCards);
			p.setHand(HandFacade.getHand(playerCards));
			hands.add(p.getHand());

		}

		return handFacade.findBestHand(hands);

	}

	/**
	 * Adds a player if there is space at the table.
	 * 
	 * @param player
	 *            , Player object to be added to the game.
	 * @return true if possible, false otherwise.
	 */
	public boolean addPlayer(Player player) {

		boolean emptyFound = false;

		for (int i = 0; i < this.players.length && !emptyFound; i++) {
			if (this.players[i] == null) {
				this.players[i] = player;
				player.setRoundAction(PlayerAction.FOLD);
				emptyFound = true;
			}
		}

		return emptyFound;
	}

	/**
	 * Adds a player to the table.
	 * 
	 * @param player
	 *            Player to be added
	 * @param seat
	 *            where the player is to be sat
	 * @return true if possible and done, false otherwise
	 */
	public boolean addPlayer(Player player, int seat) {
		if (seat >= 0 && seat < players.length) {
			this.players[seat] = player;
			player.setRoundAction(PlayerAction.FOLD);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Removes a player from the table.
	 * 
	 * @param player
	 *            , player to be removed.
	 * @return true if removed, false if player wasn't at the table.
	 */
	public boolean removePlayer(Player player) {

		boolean playerFound = false;
		int position = getPlayerPosition(player);

		if (position != -1) {
			this.players[position] = null;
			playerFound = true;
		}

		return playerFound;
	}

	/**
	 * Removes the player with the given ID from the table.
	 * 
	 * @param playerId
	 * @return true if possible and done, false otherwise
	 */
	public boolean removePlayer(int playerId) {
		return removePlayer(findPlayerById(playerId));
	}

	/**
	 * Gets player position at the table.
	 * 
	 * @param player
	 *            , a Player
	 * @return player's position, -1 if there's no match
	 */
	public int getPlayerPosition(Player player) {

		boolean found = false;
		int i;

		for (i = 0; i < 9 && !found; i++) {
			if (this.players[i] != null && this.players[i].equals(player)) {
				found = true;
			}
		}

		if (found) {
			return i;
		} else {
			return -1;
		}
	}

	/**
	 * Calculates the possible actions of the player next turn.
	 * 
	 * @return an array of boolean representing the five possible actions
	 */
	public boolean[] getPossibleButtonsNextTurn() {
		boolean[] buttons = new boolean[5];
		double lastBet = getLastBet();
		Player pTurn = getPlayerCurrentTurn();

		if (pTurn.getPlayerCurrentMoney() >= lastBet) {
			buttons[0] = true;
			buttons[2] = true;
			buttons[1] = true;
		} else {
			buttons[1] = true;
		}

		if (lastBet <= pTurn.getCurrentBidMoney()) {
			buttons[3] = true;
		}

		buttons[4] = true;

		return buttons;

	}

	/**
	 * Finds the player with the given ID.
	 * 
	 * @param playerId
	 * @return Payer if exists, null otherwise
	 */
	public Player findPlayerById(int playerId) {
		return isBounds(playerId) ? players[playerId] : null;
	}

	/**
	 * Finds out whether the given id inbounds the table.
	 * 
	 * @param playerId
	 * @return boolean
	 */
	public boolean isBounds(int playerId) {
		return playerId >= 0 && playerId <= 8;
	}

	/**
	 * Sets the player holding the turn after a round.
	 */
	public void nextTurn() {
		System.out.println("Next turn");
		int currentPos = playerCurrentTurn.getId();
		boolean found = false;
		for (int i = currentPos + 1; !found && i != currentPos; i++) {
			System.out.println("next turn: " + i);
			if (i >= players.length) {
				i = 0;
			}

			if (players[i] != null && players[i].getRoundAction() == null) {
				found = true;
				playerCurrentTurn = players[i];
			}

		}
		System.out.println("After Next turn");
	}

	/**
	 * Takes the necessary actions after a round ends.
	 */
	public void onRoundEnd() {
		setAllActionsToNull(getActivePlayers());
		collectBets();

	}

	/**
	 * Adds bets to currentTableMoney field and removes them from players'
	 * currentBidMoney.
	 */
	public void collectBets() {
		for (Player p : getActivePlayers()) {
			this.currentTableMoney += p.getCurrentBidMoney();
			p.setCurrentBidMoney(0);
		}
	}

	/**
	 * Gets a list of the non-null payers at the table.
	 * 
	 * @return
	 */
	public List<Player> getNonNullPlayers() {
		List<Player> nonNullPlayers = new ArrayList<Player>();
		for (Player p : players) {
			if (p != null) {
				nonNullPlayers.add(p);
			}
		}

		return nonNullPlayers;
	}

	/**
	 * Sets RoundAction to null for every player in the given list.
	 * 
	 * @param players
	 *            , a list of players
	 */
	public void setAllActionsToNull(List<Player> players) {
		for (Player p : players) {
			if (p != null) {
				p.setRoundAction(null);
			}
		}
	}

	/**
	 * Finds out whether the current round has ended.
	 * 
	 * @return boolean
	 */
	public boolean isRoundEnd() {
		List<Player> possibleWinners = getPossibleWinners();
		if (getPossibleWinners().size() == 1) {
			return true;
		}

		for (Player p : possibleWinners) {
			if (p.getRoundAction() == null) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Finds out whether the current game has ended.
	 * 
	 * @return boolean
	 */
	public boolean isGameEnd() {
		if (!isRoundEnd()) {
			return false;
		}

		List<Player> activePlayers = getActivePlayers();
		if (activePlayers.size() <= 1) {
			return true;
		}
		if (getBoardCards().size() == 5) {
			return true;
		}

		int nonFoldActions = 0;
		for (Player p : players) {
			if (p != null && p.getRoundAction() != PlayerAction.FOLD) {
				nonFoldActions++;
			}
		}

		return nonFoldActions == 1;
	}

	/**
	 * Takes all necessary actions after a game ands.
	 */
	public void onGameEnd() {
		Map<Integer, Double> winnerPot = getWinnerPot();
		for (Map.Entry<Integer, Double> winPot : winnerPot.entrySet()) {
			Player win = players[winPot.getKey()];
			win.setPlayerCurrentMoney(win.getPlayerCurrentMoney()
					+ winPot.getValue());
		}

		this.boardCards.clear();
		setAllActionsToNull(getNonNullPlayers());
		croupier.collectCards();
		this.currentTableMoney = 0;
	}

	/**
	 * Starts a new game.
	 */
	public void initGame() {
		setAllActionsToNull(getNonNullPlayers());
		setStarted(true);
		croupier.shuffle();
		croupier.dealCards();
		for (Player p : getNonNullPlayers()) {
			p.setCurrentBidMoney(0);
		}
		lastBet = 0;
		currentTableMoney = 0;
		if (playerCurrentTurn == null) {
			this.playerCurrentTurn = this.players[0];
		}
	}

	/**
	 * Looks for an empty seat at the table.
	 * 
	 * @return true if found, -1 otherwise
	 */
	public int findEmptySeat() {
		for (int i = 0; i < players.length; i++) {
			if (players[i] == null) {
				return i;
			}
		}

		return -1;
	}

	/**
	 * Gets the list of all the players with chances to win the game.
	 * 
	 * @return List<Player>, a list of players
	 */
	public List<Player> getPossibleWinners() {
		List<Player> possibleWinners = new ArrayList<>();
		for (Player p : players) {
			if (p != null && p.getRoundAction() != PlayerAction.FOLD) {
				possibleWinners.add(p);
			}
		}

		return possibleWinners;
	}

	/**
	 * Gets all the players who are able to take actions.
	 * 
	 * @return List<Player>, a list of players
	 */
	public List<Player> getActivePlayers() {
		List<Player> active = new ArrayList<Player>();

		for (Player p : players) {
			if (p != null && p.getRoundAction() != PlayerAction.FOLD
					&& p.getRoundAction() != PlayerAction.ALLIN) {
				active.add(p);
			}
		}

		return active;

	}

	/**
	 * Finds the player who held the previous turn.
	 * 
	 * @return Player if found, null otherwise
	 */
	public Player findLastTurnPlayer() {
		int idThisTurn = playerCurrentTurn.getId();
		Player pLastTurn = null;
		boolean found = false;

		for (int i = idThisTurn - 1; !found && i != idThisTurn; i--) {
			if (i < 0) {
				i = 8;
			}

			if (players[i] != null && players[i].getRoundAction() != null) {
				pLastTurn = players[i];
				found = true;
			}
		}

		return pLastTurn;
	}

	/**
	 * Gets clients' actions from ClientThread, modifies the game accordingly,
	 * and sends a message back to the client.
	 * 
	 * @param msg
	 *            , message from the client
	 * @param pid
	 *            , player ID
	 * @return Message to be sent back to client
	 */
	public Message consumeMessage(Message msg, int pid) {
		System.out.println("Entrando en ConsumeMessage");
		if (pid != playerCurrentTurn.getId()
				&& msg.getCode() != MessageFactory.MSG_DISCONNECT) {
			return null;
		}
		Message msgToRet = null;
		Player p = getPlayers()[pid];
		switch (msg.getCode()) {
		case MessageFactory.MSG_ALLIN:
			if (p.getPlayerCurrentMoney() + p.getCurrentBidMoney() > getLastBet()) {
				setAllActionsToNull(getActivePlayers());
			}
			setLastBet(p.getPlayerCurrentMoney() + p.getCurrentBidMoney());
			p.allin();
			msgToRet = MessageFactory.createAllInMessage(
					getCurrentTableMoney(), p.getPlayerCurrentMoney(),
					p.getCurrentBidMoney());
			break;
		case MessageFactory.MSG_CALL:
			p.call(getLastBet() - p.getCurrentBidMoney());
			msgToRet = MessageFactory.createCallMessage(getCurrentTableMoney(),
					p.getPlayerCurrentMoney(), p.getCurrentBidMoney());
			break;
		case MessageFactory.MSG_CHECK:
			p.check();
			msgToRet = MessageFactory.createCheckMessage();
			break;
		case MessageFactory.MSG_CHAT:

			break;
		case MessageFactory.MSG_FOLD:
			p.fold();
			msgToRet = MessageFactory.createFoldMessage();
			break;
		case MessageFactory.MSG_RAISE:
			RaiseMessage raise = (RaiseMessage) msg;
			double amount = raise.getAmount();
			setLastBet(amount + p.getCurrentBidMoney());
			setAllActionsToNull(getActivePlayers());
			if (amount == p.getPlayerCurrentMoney()) {
				p.allin();
				msgToRet = MessageFactory.createAllInMessage(
						getCurrentTableMoney(), p.getPlayerCurrentMoney(),
						p.getCurrentBidMoney());
			} else {
				p.raise(amount);
				msgToRet = MessageFactory.createRaiseMessage(
						getCurrentTableMoney(), p.getPlayerCurrentMoney(),
						p.getCurrentBidMoney());
			}
			break;
		case MessageFactory.MSG_DISCONNECT:
			msgToRet = MessageFactory.createDisconnectMessage();
			players[pid] = null;
			break;
		default:
			break;
		}
		if (msgToRet != null) {
			msgToRet.setPlayerId(pid);
			if (isPokerEvent(msgToRet.getCode())) {
				dao.savePokerEvent(createPokerEvent(p));
			}
		}
		return msgToRet;
	}

	public boolean isPokerEvent(int code) {
		for (int act : ACTIONS_POKER_EVENT) {
			if (act == code) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Creates the pokerEvent after the Player p did an action that should be
	 * stored. The actions that will make this method be called are FOLD, RAISE,
	 * CALL, ALL-IN, CHECK.
	 * 
	 * 
	 * @param p
	 *            The player that did the action that should be stored.
	 * @return The PokerEvent
	 */
	public PokerEvent createPokerEvent(Player p) {
		List<String> input = new ArrayList<String>();
		List<String> output = new ArrayList<String>();

		// This will hold all the player cards and the boardCards
		List<Card> cards = new ArrayList<Card>();
		cards.addAll(p.getHand().getCards());
		cards.addAll(getBoardCards());

		// For each card, convert it to the database format, which is
		// "{rank}{kind}" where
		// kind is one of "c, d, h, s"

		for (Card c : cards) {
			String cardName;
			String cardKind = null;
			switch (c.getKind()) {
			case Card.CLUBS:
				cardKind = "c";
				break;
			case Card.DIAMONDS:
				cardKind = "d";
				break;
			case Card.HEARTS:
				cardKind = "h";
				break;
			case Card.SPADES:
				cardKind = "s";
				break;
			default:
				break;
			}
			cardName = c.getRank() + cardKind;
			input.add(cardName);
		}
		// The output will be the action that the player executed.
		output.add(p.getRoundAction().toString());

		// Create and return the event.
		PokerEvent event = new PokerEvent();
		event.setInput(input);
		event.setOutput(output);

		return event;

	}

	// ---------- GETTERS AND SETTERS ----------

	public Player[] getPlayers() {
		return players;
	}

	public void setPlayers(Player[] players) {
		this.players = players;
	}

	public String getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}

	public Chat getChat() {
		return this.chat;
	}

	public List<Card> getBoardCards() {
		return boardCards;
	}

	public void setBoardCards(List<Card> boardCards) {
		this.boardCards = boardCards;
	}

	public Croupier getCroupier() {
		return croupier;
	}

	public void setCroupier(Croupier croupier) {
		this.croupier = croupier;
	}

	public boolean isStarted() {
		return isStarted;
	}

	public void setStarted(boolean isStarted) {
		this.isStarted = isStarted;
	}

	public void addToBoardCards(List<Card> newCards) {
		this.boardCards.addAll(newCards);

	}

	public static void setInstance(Game instance) {
		Game.instance = instance;
	}

	public double getLastBet() {
		return this.lastBet;
	}

	public void setLastBet(double lastBet) {
		this.lastBet = lastBet > this.lastBet ? lastBet : this.lastBet;
	}

	public boolean isMoneyBounds(double amount) {
		return false;
	}

	public double getCurrentTableMoney() {
		return currentTableMoney;
	}

	public void setCurrentTableMoney(double currentTableMoney) {
		this.currentTableMoney = currentTableMoney;
	}

	public Player getPlayerCurrentTurn() {
		return playerCurrentTurn;
	}

	public void setPlayerCurrentTurn(Player playerCurrentTurn) {
		this.playerCurrentTurn = playerCurrentTurn;
	}

}
