 /* Player.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.player;

import java.util.ArrayList;
import java.util.List;

import com.damfav.vcpoker.hand.Card;
import com.damfav.vcpoker.hand.Hand;
import com.damfav.vcpoker.hand.HandFacade;


public class Player {	
	
	protected List<PlayerStatus> currentStatus;
	protected double currentBidMoney = 0;
	protected double playerCurrentMoney = 500;
	protected Hand hand;
	protected Profile profile;
	protected int id;
	protected int position;
	protected PlayerAction roundAction;
	
	public Player(){
		currentStatus = new ArrayList<PlayerStatus>();
		
		
		profile = new Profile();
		profile.setName("ffff");
		
		currentBidMoney = 0;
	}

	
	public Player(PlayerAction lastAction, List<PlayerStatus> currentStatus,
			double currentBidMoney, double playerCurrentMoney, Hand hand,
			Profile profile) {
		this.roundAction = lastAction;
		this.currentStatus = currentStatus;
		this.currentBidMoney = currentBidMoney;
		this.playerCurrentMoney = playerCurrentMoney;
		this.hand = hand;
		this.profile = profile;
	}
	
	/**
	 * Contructor with lastAction attribute set to FOLD by default.
	 * 
	 * @param currentStatus
	 * @param currentBidMoney
	 * @param playerCurrentMoney
	 * @param hand
	 * @param profile
	 */
	public Player(List<PlayerStatus> currentStatus,
			double currentBidMoney, double playerCurrentMoney,
			Hand hand, Profile profile) {
		this.roundAction = PlayerAction.FOLD;
		this.currentStatus = currentStatus;
		this.currentBidMoney = currentBidMoney;
		this.playerCurrentMoney = playerCurrentMoney;
		this.hand = hand;
		this.profile = profile;
	}
	
	public void raise(double amount) {
		addBet(amount);
		this.roundAction = PlayerAction.RAISE;
	}

	public void call(double amount) {
		addBet(amount);
		this.roundAction = PlayerAction.CALL;
	}

	public void check() {
		this.roundAction = PlayerAction.CHECK;
	}

	public void fold() {
		this.roundAction = PlayerAction.FOLD;
	}

	public void allin() {
		addBet(this.playerCurrentMoney);
		roundAction = PlayerAction.ALLIN;
	}

	private void addBet(double amount) {
		this.playerCurrentMoney -= amount;
		this.currentBidMoney += amount;
	}
	
	
	// GETTERS AND SETTERS

	public List<PlayerStatus> getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(List<PlayerStatus> currentStatus) {
		this.currentStatus = currentStatus;
	}

	public double getCurrentBidMoney() {
		return currentBidMoney;
	}

	public void setCurrentBidMoney(double currentBidMoney) {
		this.currentBidMoney = currentBidMoney;
	}

	public double getPlayerCurrentMoney() {
		return playerCurrentMoney;
	}

	public void setPlayerCurrentMoney(double playerCurrentMoney) {
		this.playerCurrentMoney = playerCurrentMoney;
	}

	public Hand getHand() {
		return hand;
	}

	public void setHand(Hand hand) {
		this.hand = hand;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public PlayerAction getRoundAction() {
		return roundAction;
	}

	public void setRoundAction(PlayerAction roundAction) {
		this.roundAction = roundAction;
	}
	
	public int getId() {
		return this.id;
	}

	public void setId(int id){
		this.id = id;
	}
	
	public int getPosition() {
		return this.position;
	}
	
	public void setPosition(int pos) {
		this.position = pos;
	}
	
	
	public void recalculateHand(Card newCard){
		List<Card> cards = null;
		if (this.hand == null){
			cards = new ArrayList<Card>();
		}else{
			cards = this.hand.getCards();
		}
		
		cards.add(newCard);
		setHand(HandFacade.getHand(cards));
	}
	
	public void recalculateHand(List<Card> newCards){
		List<Card> cards = null;
		if (this.hand == null){
			cards = new ArrayList<Card>();
		}else{
			cards = this.hand.getCards();
		}
		cards.addAll(newCards);
		setHand(HandFacade.getHand(cards));
	}
	
	
	public void blind() {
		currentBidMoney += 5;
	}
	
	
}
