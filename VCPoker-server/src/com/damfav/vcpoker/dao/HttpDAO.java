/* HttpDAO.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.dao;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;

import com.damfav.vcpoker.game.PokerEvent;
import com.damfav.vcpoker.player.Profile;
import com.google.gson.Gson;

/**
 * An implementation of the DAO interface that access the data through http
 * using a remote webserver.
 * 
 * @author francescbautista
 * 
 */
public class HttpDAO implements DAO {

	/**
	 * The object that models an httpClient
	 */
	private HttpClient httpClient;

	/**
	 * The host url.
	 */
	private static String HOST = "damfav-vpoker.rhcloud.com";

	/**
	 * The webserver action that stores PokerEvent
	 */
	private static String ACTION_TRACK_EVENT = "/statistics/trackEventJson";

	/**
	 * The webserver action that retrieves Profile
	 */
	private static String ACTION_GET_PROFILE = "/user/getProfile";

	/**
	 * The base url for all the requests.
	 */
	private static String BASE_URL = "http://" + HOST;

	/**
	 * An instance of the Gson to avoid recreating one each time a pokerEvent
	 * has to be stored.
	 */
	private Gson gson = new Gson();

	public HttpDAO() {
		httpClient = new DefaultHttpClient();
	}

	@Override
	public synchronized void  savePokerEvent(PokerEvent ev) {
		// TODO Auto-generated method stub
		String json = gson.toJson(ev);

		try {
			httpClient = new DefaultHttpClient();
			String URLToTarget = BASE_URL + ACTION_TRACK_EVENT;
			URIBuilder builder = new URIBuilder(URLToTarget);
			System.out.println(URLToTarget);
			builder.setParameter("ev", json);
			HttpPost post = new HttpPost(builder.build());
			httpClient.execute(post);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public Profile getProfile(String email) {
		// TODO Auto-generated method stub
		return null;
	}

}
