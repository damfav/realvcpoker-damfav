/* PokerServer.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.damfav.vcpoker.game.Game;
import com.damfav.vcpoker.hand.Card;
import com.damfav.vcpoker.message.HandshakeMessage;
import com.damfav.vcpoker.message.MessageFactory;
import com.damfav.vcpoker.player.Player;

public class PokerServer {

	private ServerSocket mServer;

	public static int MSG_SEND_STATE = 10;

	public static int MSG_HANDSHAKE = 12;

	public static int MSG_GET_STATE = 9;

	public static int MSG_DISCONNECT = 8;

	private static int PORT = 16001;

	private List<Table> mTables;

	private NewClientThread mNewClientThread;

	private long gTableId = 0;

	public static void main(String args[]) {
		new PokerServer();
	}

	public PokerServer() {
		mTables = new ArrayList<>();
		try {

			mServer = new ServerSocket(PORT);
			mNewClientThread = new NewClientThread();
			mNewClientThread.start();

		} catch (IOException e) {
			System.out.println("E2");
		}
	}

	public synchronized void broadcast(List<ClientThread> clients, String input) {
		for (ClientThread c : clients) {

			// System.out.println("Sending : "+ input);
			c.getOut().println(input);

		}
	}

	public synchronized void sendMsg(ClientThread client, String input) {
		client.getOut().println(input);
	}

	public void removeTable(Table table) {
		mTables.remove(table);
	}

	public void sendHandShake(ClientThread th) {
		HandshakeMessage handShake = new HandshakeMessage();

		handShake.setSuccess(th.getmTable() != null);

		if (th.getmTable() != null) {
			handShake.setOwnPlayerSeat(th.getmTable().findEmtpySeats().get(0));
			handShake.setPlayerCurrentMoney(500);
			Game game = th.getmTable().getmGame();
			if (game.isStarted()) {
				handShake.setMoneyInTheTable(game.getCurrentTableMoney());
				handShake.setPlayerCurrentTurn(game.getPlayerCurrentTurn()
						.getId());
				List<Card> c = game.getBoardCards();
				for (int i = 0; i < c.size(); i++) {
					System.out.println("card" + i + " : " + c.get(i));
				}
				handShake.setCurrentCardsInTheTable(game.getBoardCards());
			}
			th.getOut().println(handShake.toJson());

		}

	}

	public void sendPlayersState(ClientThread th) {

		for (Player p : th.getmTable().getmGame().getPlayers()) {
			if (p != null) {
				String peerMessage = MessageFactory.createNewPeerMessage(p)
						.toJson();
				th.getOut().println(peerMessage);
			}
		}

	}

	public Table findEmptyTable() {
		for (Table tb : mTables) {
			if (tb.findEmtpySeats().size() > 0) {
				return tb;
			}
		}

		return null;
	}

	public int countTotalClients() {
		int n = 0;
		for (Table t : mTables) {
			n += t.countClients();
		}

		return n;
	}

	/**
	 * 
	 * ============================================
	 * 
	 * INNER CLASSES
	 * 
	 * ============================================
	 * 
	 */

	private class NewClientThread extends Thread {

		public void run() {

			while (true) {

				try {
					Socket cliSocket = mServer.accept();

					if (alreadyConnected(cliSocket)) {
						continue;
					}
					System.out.println(countTotalClients());
					manageClient(cliSocket);

				} catch (IOException e) {
					System.out.println("E1");
				}
			}

		}

		public boolean alreadyConnected(Socket s) {
			for (Table table : mTables) {

				for (ClientThread clTh : table.getClients()) {
					if (clTh != null
							&& clTh.getSocket().getRemoteSocketAddress()
									.equals(s.getRemoteSocketAddress())) {

						return true;

					}
				}
			}

			return false;
		}

		public void manageClient(Socket cliSocket) {

			ClientThread th = new ClientThread(cliSocket, PokerServer.this);
			String isStart = null;
			try {

				isStart = th.getIn().readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("connected");
			System.out.println("Start: " + isStart);
			System.out.println("Num tables: " + mTables.size());

			Table emptyTable = null;
			if (isStart == null || isStart.trim().equals("0")) {
				emptyTable = findEmptyTable();
			} else {
				emptyTable = new Table(gTableId++, PokerServer.this);
				mTables.add(emptyTable);
				System.out.println(mTables.size());
			}

			th.setmTable(emptyTable);

			sendHandShake(th);
			sendPlayersState(th);

			if (emptyTable != null) {
				System.out.println("Num table " + emptyTable.countClients());
				emptyTable.add(th);

				th.start();
			}

		}

	}

}
