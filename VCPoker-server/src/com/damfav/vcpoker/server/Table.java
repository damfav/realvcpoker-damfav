/* Table.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.server;

import java.util.ArrayList;
import java.util.List;

import com.damfav.vcpoker.game.Game;
import com.damfav.vcpoker.hand.Card;
import com.damfav.vcpoker.message.AddChipsMessage;
import com.damfav.vcpoker.message.GameStartMessage;
import com.damfav.vcpoker.message.Message;
import com.damfav.vcpoker.message.MessageFactory;
import com.damfav.vcpoker.message.NextTurnMessage;
import com.damfav.vcpoker.player.Player;

public class Table {

	private long id;

	private Game mGame;

	private ClientThread[] mClients = new ClientThread[9];

	private PokerServer mServer;

	@SuppressWarnings("unused")
	private Thread timerThread;

	public Table(long id, PokerServer server) {
		this.id = id;
		this.mServer = server;
		this.mGame = new Game();
	}

	public List<ClientThread> asList() {
		List<ClientThread> others = new ArrayList<>();
		for (int i = 0; i < mClients.length; i++) {
			if (mClients[i] != null) {
				others.add(mClients[i]);
			}
		}

		return others;
	}

	public List<Integer> findEmtpySeats() {
		List<Integer> emptySeats = new ArrayList<>();
		for (int i = 0; i < mClients.length; i++) {
			if (mClients[i] == null) {
				emptySeats.add(i);
			}
		}

		return emptySeats;
	}

	public void sendNextTurnMsg() {
		boolean[] possibleButtons = mGame.getPossibleButtonsNextTurn();
		double lastBet = mGame.getLastBet();
		NextTurnMessage nxtTurn = new NextTurnMessage(possibleButtons, lastBet);
		ClientThread[] clients = getClients();
		ClientThread currentTurn = clients[mGame.getPlayerCurrentTurn().getId()];
		mServer.sendMsg(currentTurn, nxtTurn.toJson());
	}

	public void add(ClientThread th) {
		List<Integer> seats = findEmtpySeats();
		if (seats.size() == 0) {
			return;
		}

		int emptySeat = seats.get(0);

		th.setPid(emptySeat);

		Player player = new Player();
		player.setId(emptySeat);
		player.getProfile().setName("player" + emptySeat);
		notifyOthers(player);

		mClients[emptySeat] = th;
		mGame.addPlayer(player, th.getPid());

		if (countClients() == 2 && !mGame.isStarted()) {
			mGame.initGame();
			sendStartMsg();
			sendNextTurnMsg();
			startTimer();
		}

	}

	public void sendStartMsg() {
		for (Player p : mGame.getActivePlayers()) {
			List<Card> pCards = p.getHand().getCards();
			GameStartMessage gStart = new GameStartMessage();
			gStart.setOwnCards(pCards);
			System.out.println(gStart.toJson());
			mClients[p.getId()].getOut().println(gStart.toJson());
		}
	}

	private void notifyOthers(Player player) {
		Message npm = MessageFactory.createNewPeerMessage(player);
		mServer.broadcast(asList(), npm.toJson());
	}

	public void remove(ClientThread th) {
		mClients[th.getPid()] = null;
		if (countClients() == 0) {
			mServer.removeTable(this);
		}
	}

	public int countClients() {
		int counter = 0;

		for (ClientThread cli : mClients) {
			if (cli != null) {
				counter++;
			}
		}

		return counter;
	}

	public void interruptTimer() {
		// timerThread.interrupt();
	}

	public void startTimer() {
		// timerThread = new Thread(){
		// public void run(){
		// try {
		// Thread.sleep(10000);
		// Message fold = MessageFactory.createFoldMessage();
		// ClientThread th = getClients()[mGame.getPlayerCurrentTurn().getId()];
		// th.returnMsg(mGame.consumeMessage(fold, th.getPid()), "");
		//
		// } catch (InterruptedException e) {
		// e.printStackTrace();
		// }
		// }
		// };
		// timerThread.start();

	}

	public ClientThread[] getClients() {
		return mClients;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Game getmGame() {
		return mGame;
	}

	public void setmGame(Game mGame) {
		this.mGame = mGame;
	}

	public void sendChips() {
		for (Player p : mGame.getNonNullPlayers()) {
			if (p.getPlayerCurrentMoney() == 0) {
				AddChipsMessage msg = new AddChipsMessage();
				msg.setPlayerId(p.getId());
				p.setPlayerCurrentMoney(200);
				mServer.broadcast(asList(), msg.toJson());
			}
		}
	}

}
