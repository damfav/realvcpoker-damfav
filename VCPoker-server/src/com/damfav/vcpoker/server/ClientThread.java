 /* ClientThread.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import com.damfav.vcpoker.game.Game;
import com.damfav.vcpoker.hand.Card;
import com.damfav.vcpoker.message.DisconnectMessage;
import com.damfav.vcpoker.message.GameEndMessage;
import com.damfav.vcpoker.message.Message;
import com.damfav.vcpoker.message.MessageFactory;
import com.damfav.vcpoker.message.RoundEndMessage;
import com.damfav.vcpoker.player.Player;

/**
 * Represents the Thread that listens to peers that try to establish a socket
 * connection with this user. Everytime a connection is established the event
 * onNewConnection will be raised.
 * 
 * @author Francesc Bautista
 * 
 */
class ClientThread extends Thread {

	private Socket socket;

	private BufferedReader in;

	private PrintWriter out;

	private boolean end;

	private int pid;

	private Table mTable;

	private PokerServer mServer;

	public ClientThread(Socket s, PokerServer mServer) {
		this.socket = s;
		this.mServer = mServer;
		try {
			s.setSoTimeout(0);
		} catch (SocketException e1) {
			e1.printStackTrace();
		}

		try {
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
		} catch (IOException e) {
			System.out.println("E3");

		}

		try {
			out = new PrintWriter(s.getOutputStream(), true);
		} catch (IOException e) {
			System.out.println("E4");
		}
	}

	/**
	 * Accepts connection until end is set to true.
	 */
	public void run() {
		// Listen while there is still place for new players to join
		// a game.
		while (!end) {
			try {
				socket.setSoTimeout(0);
			} catch (SocketException e1) {
				e1.printStackTrace();
			}
			String input = null;

			try {
				System.out.println("input: preinput");
				input = in.readLine();
				System.out.println("input: " + input);
			} catch (IOException e) {

				input = onDisconnect();
				e.printStackTrace();
			}

			// System.out.println("Recieveing : " + input);
			if (input == null) {
				System.out.println("input is null");
				input = onDisconnect();
				return;
			}

			Message msg = MessageFactory.create(input);

			if (msg != null) {
				msg.setPlayerId(pid);

				Message msgToRet = mTable.getmGame().consumeMessage(msg, pid);

				if (msgToRet != null) {
					returnMsg(msgToRet, input);
				}
			}

		}

	}

	public void returnMsg(Message msgToRet, String input) {
		List<ClientThread> clientsToSend = mTable.asList();
		Game game = mTable.getmGame();
		if (msgToRet.getCode() == MessageFactory.MSG_DISCONNECT) {
			input = onDisconnect();
		} else {

			onTurnEnd();

			msgToRet.setNextTurn(game.getPlayerCurrentTurn()
					.getId());
			System.out.println(msgToRet.toJson());
			input = msgToRet.toJson();
		}
		mServer.broadcast(clientsToSend, input);
		mTable.interruptTimer();
		mTable.startTimer();
		if (game.isStarted()){
			mTable.sendNextTurnMsg();
		}
		
		
	}

	/**
	 * THis method check all the posible things to do when a turn ends
	 */
	private void onTurnEnd() {
		final Game mGame = mTable.getmGame();
		if (mGame.isRoundEnd()) {

			if (mGame.isGameEnd()) {
				mGame.collectBets();
				mGame.setStarted(false);
				GameEndMessage gameEnd = new GameEndMessage();
				gameEnd.setPlayerId(-2);
				if (mGame.getPossibleWinners().size() > 1) {
					List<Card> l = new ArrayList<Card>();
					for (int i = mGame.getBoardCards().size(); i < 5; i++) {
						Card card = mGame.getCroupier().getOneCard();
						l.add(card);
						mGame.getBoardCards().add(card);
					}
					System.err.println("cards left to show: " + l.size());
					gameEnd.setCardsLeftToShow(l);
				}
				gameEnd.setNextTurn(mGame.getPlayerCurrentTurn().getId());

				// get the list of the winners from the game
				List<Player> winners = mGame.getThePlayerWithTheBestHand();
				List<Integer> winnersPositions = new ArrayList<Integer>();
				for (Player winn : winners) {
					winnersPositions.add(winn.getId());
				}
				gameEnd.setMoneyToPutInTheTable(mGame.getCurrentTableMoney());
				gameEnd.setWinnerPot(mGame.getWinnerPot());

				System.out.println(gameEnd.toJson());

				List<ClientThread> clientList = mTable.asList();
				mServer.broadcast(clientList, gameEnd.toJson());
				// when the game is ended call the method gameend
				mGame.onGameEnd();
				mGame.nextTurn();
				mTable.interruptTimer();
				new Thread(){
					public void run(){
						try {
							Thread.sleep(5000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						mGame.initGame();
						mTable.sendChips();
						mTable.sendStartMsg();
						mTable.sendNextTurnMsg();
						mTable.startTimer();
						
					}
				}.start();
				return;

			} else {
				// if the game is not end but the turn is ended create a new
				// card List
				List<Card> newCards = new ArrayList<Card>();

				// get the current cards in the table
				int nBoardCards = mGame.getBoardCards().size();

				// if there are nothing in the table
				if (nBoardCards == 0) {
					// flop the first 3 cards, and save it in the new list
					newCards = mGame.getCroupier().flop();

				} else if (nBoardCards < 5) {
					// if there are 4 cards in the table get a new card from the
					// stack in the croupier class
					Card c = mGame.getCroupier().getOneCard();

					// add the card in the newcards list
					newCards.add(c);
				}

				// add the card in the boardcards in the game
				mGame.addToBoardCards(newCards);

				// call the method on round end to do thing when the turn end
				mGame.onRoundEnd();
				// call the method to nextTurn to restart all the lastactions
				mGame.nextTurn();
				// get the current turn player
				Player currenTurnPlayer = mGame.getPlayerCurrentTurn();

				// call the method to update the view with the new money for the
				// current turn player

				RoundEndMessage roundEndMsg = new RoundEndMessage();
				roundEndMsg.setPlayerId(-2);
				roundEndMsg.setNewCards(newCards);
				int boardCardsState = RoundEndMessage.FLOP;
				if (mGame.getBoardCards().size() == 4) {
					boardCardsState = RoundEndMessage.TURN;
				} else if (mGame.getBoardCards().size() == 5) {
					boardCardsState = RoundEndMessage.RIVER;
				}
				
				roundEndMsg.setMoneyToPutInTheTable(mGame.getCurrentTableMoney());
				roundEndMsg.setBoardCardsState(boardCardsState);
				roundEndMsg.setNextTurn(currenTurnPlayer.getId());
				String roundEndMsgJson = roundEndMsg.toJson();
				System.out.println(roundEndMsgJson);
				mServer.broadcast(mTable.asList(), roundEndMsgJson);
			}

		} else {
			// call the method to nextTurn to restart all the lastactions
			mGame.nextTurn();
		}

	}

	public String onDisconnect() {
		System.out.println("On disconnect");
		DisconnectMessage disc = (DisconnectMessage) MessageFactory
				.createDisconnectMessage();
		disc.setPlayerId(this.pid);
		end = true;
		mTable.remove(this);
		System.out.println("Disconnected");
		if (!socket.isClosed()){
			try {
				socket.shutdownInput();
				socket.shutdownOutput();
				socket.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return disc.toJson();
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public void end() {
		end = true;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int id) {
		this.pid = id;
	}

	public PrintWriter getOut() {
		return out;
	}

	public void setOut(PrintWriter out) {
		this.out = out;
	}

	public boolean isEnd() {
		return end;
	}

	public void setEnd(boolean end) {
		this.end = end;
	}

	public Table getmTable() {
		return mTable;
	}

	public void setmTable(Table mTable) {
		this.mTable = mTable;
	}

	public BufferedReader getIn() {
		return in;
	}

}
