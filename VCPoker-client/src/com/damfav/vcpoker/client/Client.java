/* Client.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Observable;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;

import android.util.EventLog.Event;
import android.util.Log;

import com.damfav.vcpoker.message.Message;
import com.damfav.vcpoker.message.MessageFactory;
import com.damfav.vcpoker.viewcontroller.GameActivity;

/**
 * The Client class to connect to server to get information, mostly used by
 * {@link GameActivity}
 */
public class Client extends Observable {

	/**
	 * This field is used by the Log to show the class name
	 */
	private static final String TAG = "Client";

	/**
	 * The protocol to use in the connections
	 */
	private static String PROTOCOL = "http";

	/**
	 * Server host where to connect
	 */
	// private static String SERVER_HOST = "54.186.16.196";
	private static String SERVER_HOST = "10.0.2.2";

	private static String OPENSHIFT_SERVER_HOST = "damfav-vpoker.rhcloud.com";

	/**
	 * Port where the client will connect
	 */
	private static int SERVER_PORT = 16001;

	/**
	 * Event track URL
	 */
	public static final String ACTION_TRACK_EVENT = "/event/track";

	/**
	 * The url to update oyur avatar
	 */
	public static final String ACTION_UPDATE_AVATAR = "/user/updateAvatar";
	/**
	 * the HttpCLient instance
	 */
	private HttpClient httpClient = new DefaultHttpClient();

	/**
	 * how client Socket
	 */
	private Socket socket;

	/**
	 * The output used by Client
	 */
	private PrintWriter out;

	/**
	 * The input used by the CLient
	 */
	private BufferedReader in;

	/**
	 * Count of clients connect right now in the server
	 */
	private int nClients;

	/**
	 * The reading Thread
	 */
	private ReadThread readThread;

	/**
	 * A Thread pool used to write messages to other peers without blocking
	 * either the UI Thread or the readThread from the peers.
	 */
	private WriteThreadPoolAdapter writeThreadPool;

	/**
	 * Own instance used for the singleton pattern
	 */
	private static Client instance;

	/**
	 * Default contructor
	 */
	private Client() {
		writeThreadPool = new WriteThreadPoolAdapter();
	}

	/**
	 * Get instance created to get the instance of the Client
	 * 
	 * @return Return the own instance
	 */
	public static Client getInstance() {
		if (instance == null) {
			instance = new Client();
		}
		return instance;
	}

	/**
	 * This Method join a existing game
	 */
	public void joinGame() {
		// initialize the socket
		initSocket();
		sendMsg("0", null);
	}

	/**
	 * This method connect the socket to the server
	 */
	public void initSocket() {
		if (socket != null) {
			return;
		}
		try {
			// create the socket using the server host and the port
			socket = new Socket(SERVER_HOST, SERVER_PORT);

			// get the output from the server
			out = new PrintWriter(socket.getOutputStream(), true);

			// create a input reader from the server
			in = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));

			// create the reader thread and start it
			readThread = new ReadThread();
			readThread.start();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * This method create a new game with a new table
	 */
	public void startGame() {
		initSocket();
		sendMsg("1", null);
	}

	/**
	 * Returns an inputStream containing the response to that request or null if
	 * the reqeust failed.
	 * 
	 * @param action
	 *            One of the services the server offers.
	 * @return InputStream the content of the response.
	 */
	public HttpResponse request(String action, HttpEntity entity) {
		// create a request url string
		String request = PROTOCOL + "://" + OPENSHIFT_SERVER_HOST + action;

		try {
			// create a postrequest to the server
			HttpPost postRequest = new HttpPost(request);
			Header header = new BasicHeader(
					"User-Agent",
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.0.3705; .NET CLR 1.1.4322; .NET CLR 1.2.30703)");
			postRequest.setHeader(header);
			// set the list in the post request
			postRequest.setEntity(entity);
			// get a response from the server
			HttpResponse response = httpClient.execute(postRequest);

			// return the inputstream received from the server
			return response;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public void trackEvent(Event e) {
		request(Client.ACTION_TRACK_EVENT, null);
	}

	/**
	 * This method send a message to the server
	 * 
	 * @param msg
	 *            {@link Message} to send to server
	 */
	public void sendMsg(final Message msg, Runnable callback) {
		// check if the message is diferent then null
		if (msg == null) {
			return;
		}
		// send the message to server
		sendMsg(msg.toJson(), callback);

	}

	/**
	 * This method send a message to the server
	 * 
	 * @param msg
	 *            Get a String message to send to server
	 * 
	 */
	public void sendMsg(final String msg, final Runnable callback) {
		// create a new runnable to send message
		Runnable cmd = new Runnable() {

			@Override
			public void run() {
				// when the thread is running print the message
				out.println(msg);
				if (callback != null) {
					//if there are any callback to do run the callback
					callback.run();
				}
			}

		};

		// Execute the command in the pool.
		writeThreadPool.write(cmd);
	}

	public int getNClients() {
		return nClients;
	}

	/**
	 * This method is called when the user will disconect
	 */
	public void onServerDisconnect() {
		try {
			readThread.end = true;

			if (!socket.isClosed()) {
				socket.getInputStream().close();
				socket.close();
				Log.d(TAG, socket.isClosed() + " is closed");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * A Class that represents a Thread listening to the messages sent by this
	 * PeerConnection socket.
	 * 
	 * It listens to new messages until end() is called. Everytime a message is
	 * recieved the Peer's onMessageRecieved event will be raised.
	 * 
	 */
	private class ReadThread extends Thread {

		/**
		 * A flag indicating whether this thread should keep listening
		 */
		private boolean end = false;

		public void run() {

			// While an end request hasn't been requested.
			while (!end) {

				try {
					// Messages are sent by json with an end of line at the end.
					// The json messages sent cannot have new lines in the
					// middle.
					String input = in.readLine();

					Message msg = MessageFactory.create(input);
					// Raise the onMessageRecieved event passing this
					// PeerConnection
					// by parameter and the json string representation of
					// the message.
					Client.this.setChanged();
					Client.this.notifyObservers(msg);

					if (input == null) {
						return;
					}
				} catch (IOException e) {
					onServerDisconnect();
					e.printStackTrace();
				}

			}
		}

		/**
		 * Kills the thread.
		 */
		@SuppressWarnings("unused")
		public void kill() {
			end = true;
		}
	}

	/**
	 * A class that follows the Adapter pattern to adapt the ThreadPoolExecutor
	 * to the Peer class. It has a write method that executes the given message
	 * on a thread.
	 * 
	 */
	private static class WriteThreadPoolAdapter {

		/**
		 * The adapter. This pool will be used to send the messages to the
		 * connected peers.
		 */
		private ThreadPoolExecutor pool;

		/**
		 * Default options of the pool.
		 */
		static final int defaultCorePoolSize = 5;

		static final int defaultMaximumPoolSize = 10;

		static final long defaultKeepAliveTime = 600;

		final TimeUnit defaultTimeUnit = TimeUnit.SECONDS;

		final BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<Runnable>();

		public WriteThreadPoolAdapter() {
			pool = new ThreadPoolExecutor(defaultCorePoolSize,
					defaultMaximumPoolSize, defaultKeepAliveTime,
					defaultTimeUnit, workQueue);
		}

		public void write(Runnable cmd) {
			pool.execute(cmd);
		}
	}

	public static void setInstance(Client instance) {
		Client.instance = instance;
	}

}
