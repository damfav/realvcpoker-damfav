/* ProfileActivity.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.viewcontroller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.damfav.vcpoker.client.Client;
import com.damfav.vcpoker.model.Card;
import com.damfav.vcpoker.model.Profile;
import com.damfav.vcpoker_client.R;

/**
 * The Game login activity, with the different login options
 */
public class ProfileActivity extends Activity implements OnClickListener,
		DialogInterface.OnClickListener {

	private static final int ACTIVITY_CHOOSE_FILE = 0;

	/**
	 * Shared preference instance to save information and recover the
	 * information
	 */
	private SharedPreferences mPrefs;

	/**
	 * Current logged profile
	 */
	private Profile userProfile;

	/**
	 * This field is used to set a costumize font
	 */
	private Typeface typeFace;

	/**
	 * Current activity name to show in the Log
	 */
	private String TAG = "Profile Activity";

	/**
	 * User email
	 */
	private String email;

	/**
	 * Dialog to use in the username update proccess
	 */
	private Dialog userNamechangeDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);
		Log.d(TAG, "Starting onCreate");
		// get the sharedpreference with the login preference file
		mPrefs = getSharedPreferences(Constants.LOGINPREFERENCEFILE,
				MODE_PRIVATE);
		Log.d(TAG, "premail");
		System.err.println(mPrefs.getString(Constants.LOGGEDEMAIL, ""));
		Log.d(TAG, "aftermail");

		// set all the listeners in this activity
		setListeners();

		// load the costum typeface
		// typeFace = Typeface.createFromAsset(getResources().getAssets(),
		// "font/mrsmonster.ttf");

		// get all the texts in the profile and set the the new typeface
		TextView textViewCurrentMoney = (TextView) findViewById(R.id.userInfo_tv_CurrentMoneyLabel);
		textViewCurrentMoney.setTypeface(typeFace);

		TextView textViewBiggestWin = (TextView) findViewById(R.id.userInfo_tv_BiggestWinLabel);
		textViewBiggestWin.setTypeface(typeFace);

		TextView textViewTotalWins = (TextView) findViewById(R.id.userInfo_tv_totalsWinLabel);
		textViewTotalWins.setTypeface(typeFace);

		TextView textViewBestHand = (TextView) findViewById(R.id.userInfo_tv_BesthandLabel);
		textViewBestHand.setTypeface(typeFace);

	}

	/**
	 * THis method put listeners in all the buttons in the profile activity
	 */
	private void setListeners() {
		Log.d(TAG, "Entrying in setListeners");
		findViewById(R.id.activity_profile_btn_playnow)
				.setOnClickListener(this);
		findViewById(R.id.activity_profile_btn_startGame).setOnClickListener(
				this);
		findViewById(R.id.activity_profile_btn_addMoney).setOnClickListener(
				this);
		findViewById(R.id.activity_profile_btn_settings).setOnClickListener(
				this);
		findViewById(R.id.activity_profile_btn_help).setOnClickListener(this);
		findViewById(R.id.activity_profile_btn_logout).setOnClickListener(this);
		findViewById(R.id.userInfo_iv_playerProfileImage).setOnClickListener(
				this);
		findViewById(R.id.userInfo_tv_Username).setOnClickListener(this);

	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "Entrying in onResume");
		// get the logged email
		email = mPrefs.getString(Constants.LOGGEDEMAIL, "");
		// get te information of the user from the server
		Log.d("Email is", "email " + email);
		getPlayerProfile();

	}

	@Override
	protected void onPause() {
		// when the user enter in on pause
		Log.d(TAG, "Entrying in onPause");

		super.onPause();
	}

	/**
	 * Picks player profile from server.
	 */
	private void getPlayerProfile() {

		// Create a new server access task
		GetPlayerProfileTask rt = new GetPlayerProfileTask();

		if (!mPrefs.getString(Constants.LOGINPREFERENCEKEY, "").equals(
				Constants.GUESTLOGIN)) {
			email = mPrefs.getString(Constants.LOGGEDEMAIL, "");
		}
		String[] paramsToGetProfile = { email };
		// execute the ServerAccessTask
		rt.execute(paramsToGetProfile);

	}

	/**
	 * This method load the information in the view from the userprofile
	 * information
	 */
	private void setPlayerInfo() {
		Log.d(TAG, "Entrying in setPlayerInfo");
		// get the view to change and get the information from the userProfile
		// and save it into the profile view
		TextView textViewUsername = (TextView) findViewById(R.id.userInfo_tv_Username);
		textViewUsername.setText(userProfile.getName());
		textViewUsername.setTypeface(typeFace);

		TextView textViewTotalMoney = (TextView) findViewById(R.id.userInfo_tv_CurrentMoney);
		textViewTotalMoney.setText("" + userProfile.getTotalMoney());
		textViewTotalMoney.setTypeface(typeFace);

		TextView textViewBiggestWin = (TextView) findViewById(R.id.userInfo_tv_BiggestWin);
		textViewBiggestWin.setText("" + userProfile.getBiggestWin());
		textViewBiggestWin.setTypeface(typeFace);

		TextView textViewTotalWins = (TextView) findViewById(R.id.userInfo_tv_totalsWin);
		textViewTotalWins.setText("" + userProfile.getTotalWins());
		textViewTotalWins.setTypeface(typeFace);

		// get the userProfile bestHand
		List<Card> list = userProfile.getBestHand();

		// get the linearlayout from the profile view
		LinearLayout bestHandLinearLayout = (LinearLayout) findViewById(R.id.profileActivityCardListLayoutID);

		for (int i = 0; i < 5; i++) {
			String cardName;
			// check if the user have already the saved cards
			if (this.userProfile.getBestHand() != null) {
				// for each iteration, get the card
				Card card = list.get(i);

				// get the name of the card to show
				cardName = Utilities.getCardName(card);
			} else {
				// otherwise just get the back cover image
				cardName = Utilities.getCardName(null);
			}

			// create a drawable with to set in the image view
			Drawable d = getResources().getDrawable(
					getResources().getIdentifier(cardName, "drawable",
							getApplicationContext().getPackageName()));

			// get the imageview of the cards
			ImageView currentCardImageView = ((ImageView) bestHandLinearLayout
					.getChildAt(i));

			// save the hold height of the cards
			int heOld = currentCardImageView.getLayoutParams().height;

			// set the drawable in the imageview
			currentCardImageView.setImageDrawable(d);

			// set the old height to the cards
			currentCardImageView.getLayoutParams().height = heOld;
		}

	}

	@Override
	public void onClick(View v) {
		Log.d(TAG, "Entrying in onClick");

		// switch the view id depending the butotn pressed
		switch (v.getId()) {
		case R.id.activity_profile_btn_playnow:
			// if the user has selected the play now option create a intent to
			// the next activity
			Intent i = new Intent(this, GameActivity.class);

			// put the user profile id in the intent
			i.putExtra(Constants.USERPROFILEIDKEY, 5);

			// start the next activity
			startActivity(i);

			break;

		case R.id.activity_profile_btn_startGame:

			// if the user has selected the start game option create a intent to
			// the next activity
			i = new Intent(getApplicationContext(), GameActivity.class);

			// put the user profile id in the intent
			i.putExtra(Constants.USERPROFILEIDKEY, 5);

			// put a boolean to tell about the user wants to start a new game
			// in the intent
			i.putExtra(Constants.START_GAMEKEY, true);

			// start the next activity
			startActivity(i);

			break;
		case R.id.activity_profile_btn_addMoney:
			// TODO implementation pending to do
			Toast.makeText(this, "Pendiente...", Toast.LENGTH_SHORT).show();
			break;
		case R.id.activity_profile_btn_settings:
			// TODO implementation pending to do
			Toast.makeText(this, "Currently there are no settings",
					Toast.LENGTH_SHORT).show();
			break;
		case R.id.activity_profile_btn_help:
			// when the user select the help button start intent to a extern web
			// to learn about poker
			Intent browserIntent = new Intent(Intent.ACTION_VIEW,
					Uri.parse("http://www.pokerlistings.com/poker-rules"));

			// start intent with a default browser
			startActivity(browserIntent);

			break;
		case R.id.activity_profile_btn_logout:
			// get a editor from the shared prefences
			Editor e = mPrefs.edit();

			// remove the login preference from the shared preferences
			e.remove(Constants.LOGINPREFERENCEKEY).commit();

			// call the on back pressed
			onBackPressed();

			break;
		case R.id.userInfo_iv_playerProfileImage:
			Intent chooseFile;
			// Intent intentC;
			chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
			chooseFile.setType("image/*");
			// chooseFile.setType("file/*");
			// intent = Intent.creatCeChooser(chooseFile, "Choose a file");
			startActivityForResult(chooseFile, ACTIVITY_CHOOSE_FILE);
			break;
		case R.id.userInfo_tv_Username:
			// Set up the input with the edittext id
			final EditText input = new EditText(this);
			input.setText(userProfile.getName());
			input.setId(Constants.NEWUSERNAMEEDITTEXT);

			// create the builder for the dialog
			AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this)
					.setTitle("Update username")
					.setMessage("Insert your new username:").setView(input);

			// set the positive and negative buttons
			dialogBuilder.setPositiveButton("Ok", this);
			dialogBuilder.setNegativeButton("Cancel", this);

			// show the dialog
			userNamechangeDialog = dialogBuilder.show();
			break;
		default:
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case ACTIVITY_CHOOSE_FILE:
			if (resultCode == RESULT_OK) {
				Uri uri = data.getData();
				String filePath = uri.getPath();
				Log.d(TAG, filePath);
				updateAvatar(uri);
			}
			break;
		default:
			break;
		}
	}

	/**
	 * Updates profile avatar from client.
	 * 
	 * @param uri
	 *            , avatar image uri
	 */
	private void updateAvatar(Uri uri) {
		UpdateAvatarTask task = new UpdateAvatarTask();
		String realPath = getRealPathFromURI(uri);

		task.execute(new String[] { realPath });
	}

	/**
	 * Gets real path from uri.
	 * 
	 * @param contentUri
	 * @return String, real path
	 */
	public String getRealPathFromURI(Uri contentUri) {
		String path = null;
		String[] proj = { MediaStore.MediaColumns.DATA };
		Cursor cursor = getContentResolver().query(contentUri, proj, null,
				null, null);
		if (cursor.moveToFirst()) {
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
			path = cursor.getString(column_index);
		}
		cursor.close();
		return path;
	}

	/**
	 * Holds the avatar updating process.
	 */
	private class UpdateAvatarTask extends AsyncTask<String, Void, Integer> {

		@Override
		protected Integer doInBackground(String... params) {
			int resCode = 0;
			String realPath = params == null ? null : params[0];
			// File file = new File(filePath);
			// Log.e("Ff", file.getName());
			MultipartEntityBuilder entBuilder = MultipartEntityBuilder.create();

			entBuilder.addPart("avatar", new FileBody(new File(realPath)));

			entBuilder.addTextBody("user_email", email);
			// entBuilder.addBinaryBody(name, stream)
			HttpResponse resp = Client.getInstance().request(
					Client.ACTION_UPDATE_AVATAR, entBuilder.build());
			// get the response code from the server
			resCode = resp.getStatusLine().getStatusCode();
			Log.e("err", "" + resCode);

			return resCode;
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);
			if (result == Constants.OK) {
				Toast.makeText(ProfileActivity.this, "Success",
						Toast.LENGTH_SHORT).show();
			}
		}

	}

	/**
	 * Holds the Username updating process.
	 */
	private class UpdateUsernameTask extends AsyncTask<String, Void, Integer> {

		@Override
		protected Integer doInBackground(String... params) {
			int resCode = 0;

			String newUsername = params[0];

			String targetURL = "http://damfav-vpoker.rhcloud.com/user/updateProfile";

			// create a post connection to the server
			HttpPost post = new HttpPost(targetURL);
			Header header = new BasicHeader(
					"User-Agent",
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.0.3705; .NET CLR 1.1.4322; .NET CLR 1.2.30703)");
			post.setHeader(header);

			// create a client http connection to execute the post request
			HttpClient client = new DefaultHttpClient();

			try {
				// create a list of NameValuePair with the user information to
				// send to the server
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						1);

				nameValuePairs.add(new BasicNameValuePair("user_email", email));
				nameValuePairs.add(new BasicNameValuePair("username",
						newUsername));

				// set the list in the post request
				post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// execute the client with the post request and get the response
				HttpResponse response = client.execute(post);

				// get the response code from the server
				resCode = response.getStatusLine().getStatusCode();
				Log.d(TAG, "" + resCode);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return resCode;
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);
			if (result == Constants.OK) {
				Toast.makeText(ProfileActivity.this,
						"Successully updated the username", Toast.LENGTH_SHORT)
						.show();
			}
		}

	}

	/**
	 * Holds the player profile getting process.
	 */
	private class GetPlayerProfileTask extends AsyncTask<String, Void, Integer> {

		/**
		 * In this field has to save the user email
		 */
		private String email = "";

		private HttpResponse response;

		private ProgressDialog dialog;

		@Override
		protected void onPreExecute() {
			this.dialog = ProgressDialog.show(
					ProfileActivity.this,
					"Loading...",
					"Getting your profile... "
							+ System.getProperty("line.separator")
							+ "Please wait", true, false);
			super.onPreExecute();
		}

		@Override
		protected Integer doInBackground(String... params) {
			Log.d(TAG, "Entrying in DoInBackground");

			if (params != null) {
				email = params[0];
			}

			// target url where to connect to connect
			String targetURL = "http://damfav-vpoker.rhcloud.com/user/getProfile";

			// create a post connection to the server
			HttpPost post = new HttpPost(targetURL);
			Header header = new BasicHeader(
					"User-Agent",
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.0.3705; .NET CLR 1.1.4322; .NET CLR 1.2.30703)");
			post.setHeader(header);

			// create a client http connection to execute the post request
			HttpClient client = new DefaultHttpClient();

			// create a new response code
			int resCode = 0;
			try {
				// create a list of NameValuePair with the user information to
				// send to the server
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("email", email));

				// set the list in the post request
				post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// execute the client with the post request and get the response
				response = client.execute(post);

				// get the response code from the server
				resCode = response.getStatusLine().getStatusCode();
				Log.e("err", "" + resCode);
			} catch (IOException e) {
				e.printStackTrace();
			}
			switch (resCode) {
			case Constants.OK:
				Log.d(TAG, "Entrying in onPostExecute OK");
				String profileJson = "";
				try {
					InputStream in = response.getEntity().getContent();
					int code = 0;

					while ((code = in.read()) != 10) {
						profileJson += Character.toString((char) code);
					}
					Log.e(TAG, profileJson);
					ByteArrayOutputStream bao = new ByteArrayOutputStream();

					while ((code = in.read()) != -1) {
						bao.write(code);
					}
					final Bitmap bm = BitmapFactory.decodeByteArray(
							bao.toByteArray(), 0, bao.size());
					if (bm != null) {
						Log.e(TAG, "Bitmap not null");
						final ImageView userImg = (ImageView) findViewById(R.id.userInfo_iv_playerProfileImage);
						userImg.post(new Runnable() {

							@Override
							public void run() {
								userImg.setImageBitmap(bm);
							}

						});

					}
					// profileJson = EntityUtils.toString(response.getEntity());
				} catch (ParseException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				Log.d(TAG, profileJson);
				System.err.println(profileJson);
				JSONObject jobj = null;
				try {
					jobj = new JSONObject(profileJson);
				} catch (JSONException e) {
					e.printStackTrace();
				}

				userProfile = Profile.fromJson(jobj);
				// userProfile = new Gson().fromJson(profileJson,
				// Profile.class);

				break;
			default:
				break;

			}
			// return the response code from the server
			return resCode;
		}

		@Override
		protected void onPostExecute(Integer result) {
			Log.d(TAG, "Entrying in onPostExecute");
			Log.d(TAG, "HttpResponse code: " + result);
			setPlayerInfo();
			dialog.dismiss();
			switch (result.intValue()) {
			case Constants.ERROR:
				// create a toast showing the error
				Toast.makeText(ProfileActivity.this, "Connection Error",
						Toast.LENGTH_SHORT).show();
				onBackPressed();
				break;
			default:
				break;
			}
		}

	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		switch (which) {
		case DialogInterface.BUTTON_POSITIVE:
			// get the edit text from the dialog
			EditText ed = (EditText) userNamechangeDialog
					.findViewById(Constants.NEWUSERNAMEEDITTEXT);

			// get the value added by user
			String value = ed.getText().toString();

			// create the update username task
			UpdateUsernameTask uut = new UpdateUsernameTask();

			// put the new username
			String[] paramsToSetProfile = { value };

			// execute the Update Username task
			uut.execute(paramsToSetProfile);

			// resume again the activity to load the change
			ProfileActivity.this.onResume();
			break;

		case DialogInterface.BUTTON_NEGATIVE:

			// hide the dialog
			dialog.dismiss();
			break;
		default:
			break;
		}
	}
}
