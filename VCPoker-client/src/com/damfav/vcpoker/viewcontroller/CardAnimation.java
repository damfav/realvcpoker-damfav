/* CardAnimation.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.viewcontroller;

import java.util.List;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;

/**
 * Card animation to show the dealing of the cards to the received users
 */
public class CardAnimation implements AnimationListener {

	/**
	 * PlayerViews to get cards.
	 */
	private List<View> recievers;
	
	/**
	 * Index where to start dealing.
	 */
	private int index;
	
	/**
	 * Card sender view.
	 */
	private View sender;
	
	/**
	 * 
	 */
	private AnimationListener callback;
	private ViewGroup root;

	@SuppressWarnings("unchecked")
	public CardAnimation(List<? extends View> players, View sender,
			AnimationListener callback, ViewGroup root) {
		this.recievers = (List<View>) players;
		this.sender = sender;
		this.callback = callback;
		this.root = root;

	}

	/**
	 * Creates the animation from the sender to the specified view.
	 * 
	 * @param v, specified view
	 * @return the animation
	 */
	public TranslateAnimation get(View v) {
		int[] locV = new int[2];
		v.getLocationInWindow(locV);
//		locV[0] = v.getLeft();
//		locV[1] = v.getTop();
		int[] senderLoc = new int[2];
		sender.getLocationInWindow(senderLoc);
		if (senderLoc[0] == 0){
			senderLoc[0] = root.getWidth() / 2;
			senderLoc[1] = root.getHeight() / 2;
		}
//		senderLoc[0] = sender.getLeft();
//		senderLoc[1] = sender.getTop();
		TranslateAnimation trans = new TranslateAnimation(0, locV[0]
				- senderLoc[0], 0, locV[1] - senderLoc[1]);
		trans.setDuration(500);
		trans.setAnimationListener(this);
		return trans;
	}

	/**
	 * Deals a card from the sender to the receiver view.
	 * 
	 * @param v, receiver view
	 */
	public void giveCard(View v) {
		sender.startAnimation(get(v));
		index++;
	}

	/**
	 * Runs the animation.
	 */
	public void run() {
		giveCard(recievers.get(0));
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		if (index < recievers.size()) {
			giveCard(recievers.get(index));
		} else {
			callback.onAnimationEnd(animation);
		}
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
	}

	@Override
	public void onAnimationStart(Animation animation) {
		callback.onAnimationStart(animation);
	}

}
