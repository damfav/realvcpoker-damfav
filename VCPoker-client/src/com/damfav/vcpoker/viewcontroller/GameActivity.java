/* GameActivity.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.viewcontroller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.damfav.vcpoker.model.Card;
import com.damfav.vcpoker.view.sound.SoundManager;
import com.damfav.vcpoker_client.R;

/**
 * The Game table class, This class contains the table to play the game
 */
public class GameActivity extends Activity {

	/**
	 * Own player GameView instance
	 */
	private GamePlayerView ownPlayerLayout;

	/**
	 * Current money at the table textview
	 */
	private TextView tableCurrentMoney;

	/**
	 * Player view of all the players at the table
	 */
	private List<GamePlayerView> playersList;

	/**
	 * Current player's position at the table
	 */
	private int currentUserPos;

	/**
	 * Controller instance to access many method and calls
	 */
	private GameController controller;

	/**
	 * The progress bar to show to the user how much time left he has to select
	 */
	private ProgressBar timeProgressbar;

	/**
	 * The seek bar to get the price to raise in the raise button
	 */
	private SeekBar seekBar;

	/**
	 * AlertDialog where to show the seek bar to raise the bet
	 */
	private AlertDialog mRaiseDialog;

	/**
	 * ProgressDialog to give information to the user while the game is
	 * connecting
	 */
	private ProgressDialog progressDialog;

	/**
	 * All he buttons in the screen
	 */
	private Button callBtn, foldBtn, checkBtn, raiseBtn, allInBtn;
	private Button[] buttons;

	/**
	 * The instance of the soundManager to play a sound
	 */
	private SoundManager soundM;

	/**
	 * The root element of the view.
	 */
	private RelativeLayout mRoot;

	private ImageView mCardSender;

	/**
	 * Current activity name to show in the Log.
	 */
	private static final String TAG = "GameActivity";

	/**
	 * Own player's current money.
	 */
	private double userCurrentMoney;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);

		// initialize fields
		this.seekBar = new SeekBar(this);
		this.soundM = new SoundManager(this);
		this.timeProgressbar = (ProgressBar) findViewById(R.id.activity_game_progress_timebar);
		this.timeProgressbar.setMax(10);
		this.timeProgressbar.setProgress(this.timeProgressbar.getMax());
		this.mRoot = (RelativeLayout) findViewById(R.id.activity_game_root);
		// Create a progress Dialog to give information to user, about hes
		// connecting to the server
		createProgressDialog();

		// get the intent from the previous activity
		Intent receivedIntent = getIntent();

		// Receive the user profile id
		int userID = receivedIntent.getIntExtra(Constants.USERPROFILEIDKEY, -1);

		// create a boolean to get if the game is already started
		boolean startGame = receivedIntent.getBooleanExtra(
				Constants.START_GAMEKEY, false);

		// initialize the controller given the 3 parameters, the gameactivity,
		// userid, if the gave is started
		this.controller = new GameController(this, userID, startGame);

		// the players from the controller
		// this.players = this.controller.getPlayers();

		// get all the game view from the activity
		getAllThePlayersViewList();

		// put all the on click listeners in the buttons
		putOnClickListeners();

		// fill the buttons array to be able to identify a button by its pos
		fillButtonsArray();
		createCardSender();
		// build a alert dialog to put a seek bar in
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		// set the seekbar in the alert dialog
		builder.setView(this.seekBar);

		// put a botton and litener in the raise seekbar
		builder.setPositiveButton("Raise", this.controller);

		// create the alert dialog
		this.mRaiseDialog = builder.create();
		Log.i(TAG, "Creating....");

		// get the ownPlayerlayout from the player list with the current own
		// player seat pos
		this.ownPlayerLayout = this.playersList.get(this.currentUserPos);

		// initialize the table current money
		this.tableCurrentMoney = (TextView) findViewById(R.id.activity_game_tv_tableCurrentMoney);

	}

	private void createCardSender() {
		mCardSender = new ImageView(this);
		mCardSender.setImageResource(R.drawable.back);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				40, 40);
		params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
		mRoot.addView(mCardSender, params);
		int[] location = new int[2];
		mCardSender.getLocationOnScreen(location);
		Log.e(TAG, location[0] + " " + location[1]);

	}

	/**
	 * Fills the buttons array with all possible buttons.
	 */
	private void fillButtonsArray() {
		buttons = new Button[5];
		buttons[0] = callBtn;
		buttons[1] = allInBtn;
		buttons[2] = raiseBtn;
		buttons[3] = checkBtn;
		buttons[4] = foldBtn;
	}

	/**
	 * Creates the progress dialog.
	 */
	private void createProgressDialog() {
		this.progressDialog = ProgressDialog.show(this, "Loading...",
				"connecting", true, false);
		this.progressDialog.setCancelable(true);
		this.progressDialog.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {
				Log.i(TAG, "Cancelado!");
				onBackPressed();
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		mCardSender.setVisibility(View.GONE);

	}

	/**
	 * Deals cards to all active players using CardAnimation.
	 */
	public void dealCards() {

		List<GamePlayerView> playersToDeal = new ArrayList<GamePlayerView>();
		for (int i = 0; i < playersList.size(); i++) {
			if (playersList.get(i).isUsedLayout()) {
				playersToDeal.add(playersList.get(i));
			}
		}
		mCardSender.setVisibility(View.VISIBLE);
		CardAnimation anim = new CardAnimation(playersToDeal, mCardSender,
				new AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {
					}

					@Override
					public void onAnimationRepeat(Animation animation) {
					}

					@Override
					public void onAnimationEnd(Animation animation) {

						mCardSender.setVisibility(View.GONE);
					}
				}, mRoot);

		anim.run();

	}

	/**
	 * Set's the own player position
	 * 
	 * @param pos
	 *            position of the own player
	 */
	public void setOwnPosition(int pos) {
		this.ownPlayerLayout = this.playersList.get(pos);
	}

	/**
	 * Put the player max money in the seek bar and show the raise money dialog
	 */
	public void showRaiseDialog(final double min) {

		this.seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				if (progress < min) {
					seekBar.setProgress((int) Math.floor(min));
				}

			}
		});

		this.seekBar.setMax((int) Math.floor(userCurrentMoney));
		this.mRaiseDialog.show();
	}

	/**
	 * Put all the onclick listeners into the controller to control the listener
	 */
	private void putOnClickListeners() {
		this.callBtn = (Button) findViewById(R.id.activity_game_btn_call);
		this.allInBtn = (Button) findViewById(R.id.activity_game_btn_allin);
		this.raiseBtn = (Button) findViewById(R.id.activity_game_btn_raise);
		this.checkBtn = (Button) findViewById(R.id.activity_game_btn_check);
		this.foldBtn = (Button) findViewById(R.id.activity_game_btn_fold);

		this.callBtn.setOnClickListener(this.controller);
		this.allInBtn.setOnClickListener(this.controller);
		this.raiseBtn.setOnClickListener(this.controller);
		this.foldBtn.setOnClickListener(this.controller);
		this.checkBtn.setOnClickListener(this.controller);

	}

	/**
	 * Gets all the playerView into the field playerList
	 */
	private void getAllThePlayersViewList() {
		this.playersList = new ArrayList<GamePlayerView>(
				Constants.MAXTABLEPLAYERS);
		this.playersList.add((GamePlayerView) findViewById(R.id.gamePlayer01));
		this.playersList.add((GamePlayerView) findViewById(R.id.gamePlayer02));
		this.playersList.add((GamePlayerView) findViewById(R.id.gamePlayer03));
		this.playersList.add((GamePlayerView) findViewById(R.id.gamePlayer04));
		this.playersList.add((GamePlayerView) findViewById(R.id.gamePlayer05));
		this.playersList.add((GamePlayerView) findViewById(R.id.gamePlayer06));
		this.playersList.add((GamePlayerView) findViewById(R.id.gamePlayer07));
		this.playersList.add((GamePlayerView) findViewById(R.id.gamePlayer08));
		this.playersList.add((GamePlayerView) findViewById(R.id.gamePlayer09));

	}

	/**
	 * Turns the cards to show to the user
	 * 
	 * @param flopCards
	 *            List of cards to show to the user
	 */
	public void showCards(List<Card> flopCards) {
		if (flopCards == null) {
			return;
		}
		// Get the Linear layout they cards are
		LinearLayout cards = (LinearLayout) findViewById(R.id.activity_game_ll_cards);

		for (int i = 0; i < flopCards.size(); i++) {
			// for each child of the linearlayout get the view, and change the
			// image of the imageview into the fiven card image
			ImageView img = (ImageView) cards.getChildAt(i);
			changeCardImageView(img, flopCards.get(i));
		}

	}

	/**
	 * Show a card depending if is the turn or river
	 * 
	 * @param card
	 *            Card to show in the imageview
	 * @param isTurn
	 *            Boolean true if is the turn and otherwise false if is river
	 *            card
	 */
	public void showACard(Card card, boolean isTurn) {
		// create a variable of imageview
		ImageView cardImageView = null;

		if (isTurn) {
			// if is the turn card get the 4 card in the table
			cardImageView = (ImageView) findViewById(R.id.activity_game_iv_tableCard4);
		} else {
			// otherwise if is the River card get the 5 card in the table
			cardImageView = (ImageView) findViewById(R.id.activity_game_iv_tableCard5);
		}

		// change image of the card with the given card
		changeCardImageView(cardImageView, card);
	}

	/**
	 * Shows the cards not yet unveiled at game end.
	 * 
	 * @param cards
	 *            cards left to show
	 */
	public void showCardsLeft(List<Card> cards) {
		System.out.println("cards size: " + cards.size());
		// Get the Linear layout they cards are
		LinearLayout cardsInTheTable = (LinearLayout) findViewById(R.id.activity_game_ll_cards);
		int j = 0;
		for (int i = 5 - cards.size(); i < 5; i++) {
			// for each child of the linearlayout get the view, and change the
			// image of the imageview into the fiven card image
			ImageView img = (ImageView) cardsInTheTable.getChildAt(i);
			changeCardImageView(img, cards.get(j));
			j++;
		}

	}

	/**
	 * Show the cards given by parameter in the player own layout
	 * 
	 * @param cards
	 *            own player's cards
	 */
	public void showOwnCards(List<Card> cards) {

		// change the image of the userCard1 to the first element of the list
		changeCardImageView(
				(ImageView) this.ownPlayerLayout.findViewById(R.id.userCard1),
				cards.get(0));

		// change the image of the userCard2 with the second element of the list
		changeCardImageView(
				(ImageView) this.ownPlayerLayout.findViewById(R.id.userCard2),
				cards.get(1));
	}

	/**
	 * Changes the image of given Imageview with the given card
	 * 
	 * @param imageView
	 *            Imageview where to change the image
	 * @param card
	 *            Card to show in the screen
	 */
	public void changeCardImageView(ImageView imageView, Card card) {
		// get the card name from the utilities class
		String cardName = Utilities.getCardName(card);

		// create a drawable with the cardname
		Drawable d = getResources().getDrawable(
				getResources().getIdentifier(cardName, "drawable",
						getApplicationContext().getPackageName()));

		// set the image in the imageview
		imageView.setImageDrawable(d);

	}

	/**
	 * This method hides all the cards in the table
	 */
	public void hideBoardCards() {
		// Get the Linear layout they cards are
		LinearLayout cards = (LinearLayout) findViewById(R.id.activity_game_ll_cards);

		for (int i = 0; i < cards.getChildCount(); i++) {
			// for each child of the linearlayout get the view, and change the
			// image of the imageview in to null
			ImageView img = (ImageView) cards.getChildAt(i);
			changeCardImageView(img, null);
		}
	}

	/**
	 * Set the money in the table with a "cool design"
	 * 
	 * @param amount
	 *            Money to set the View
	 */
	public void setCurrentTableMoney(double amount) {
		this.tableCurrentMoney.setText("" + amount);
	}

	/**
	 * Sets the money of the user
	 * 
	 * @param seat
	 *            The seat number of the user where set the money
	 * @param bidMoney
	 *            Money to set the in the table
	 * @param currentUserMoney
	 *            Money to set in the user layout
	 */
	public void setPlayerMoney(int seat, double bidMoney,
			double currentUserMoney) {
		GamePlayerView gpv = playersList.get(seat);
		gpv.setUserBidMoney(bidMoney);
		gpv.setUserMoney(currentUserMoney);
	}

	/**
	 * Play the sound depending in the parameter int
	 * 
	 * @param soundID
	 *            the sound ID, selected from the Constants class, for example:
	 *            Constants.BET_RAISE_SOUND_ID
	 */
	public void playSound(int soundID) {
		Log.i(TAG, "playing sound...");
		this.soundM.playSound(soundID);
	}

	/**
	 * Enable or Disable all the buttons depending in the parameter on
	 * 
	 * @param on
	 */
	public void enableButtons(boolean on) {
		int visiblityValue = View.VISIBLE;
		if (!on) {
			visiblityValue = View.GONE;
		}
		this.callBtn.setVisibility(visiblityValue);
		this.foldBtn.setVisibility(visiblityValue);
		this.checkBtn.setVisibility(visiblityValue);
		this.raiseBtn.setVisibility(visiblityValue);
		this.allInBtn.setVisibility(visiblityValue);
	}

	/**
	 * Sets the progress bar time to show the user about the time left to end
	 * turn
	 * 
	 * @param time
	 *            time to put in the progressbar
	 */
	public void setProgressBarTime(int time) {
		this.timeProgressbar.setProgress(time);

	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.e(TAG, "PAUSING");
		this.soundM.cleanup();
		this.controller.disconnect();
	}

	/**
	 * Adds a playerView to playersList field with username and money.
	 * 
	 * @param seat
	 * @param username
	 * @param userMoney
	 */
	public void addPlayer(int seat, String username, double userMoney) {
		GamePlayerView playerView = this.playersList.get(seat);
		playerView.getTextViewUserName().setText(username);
		playerView.getImageViewUserProfilePicture().setImageResource(
				R.drawable.viejo1);
		playerView.getTextViewUserMoney().setText("" + userMoney);
		playerView.setVisibility(View.VISIBLE);
		playerView.setUsedLayout(true);
	}

	/**
	 * Removes a playerView from playersList field.
	 * 
	 * @param seat
	 *            where the playerView is
	 */
	public void removePlayer(int seat) {
		GamePlayerView viewToRmv = this.playersList.get(seat);
		viewToRmv.setVisibility(View.INVISIBLE);
		viewToRmv.clear();
	}

	/**
	 * Shows the winner name with a Toast
	 * 
	 * @param username
	 *            The winner name
	 */
	public void showWinner(String username) {
		Toast.makeText(this, "The winner is: " + username, Toast.LENGTH_LONG)
				.show();

	}

	/**
	 * Sets players bid money to zero after each round.
	 */
	public void resetPlayersBidMoney() {
		for (int i = 0; i < playersList.size(); i++) {
			playersList.get(i).setUserBidMoney(0);
		}

	}

	public void doWinAnimation(Map<Integer, Double> winnerPot) {
		// TODO Auto-generated method stub
		StringBuilder toastText = new StringBuilder();
		for (Map.Entry<Integer, Double> entry : winnerPot.entrySet()) {
			toastText.append(entry.getKey() + " - " + entry.getValue());
		}

		Toast.makeText(this, toastText.toString(), Toast.LENGTH_SHORT).show();
		// List<View> winners = new ArrayList<View>();
		// List<View> potViews = new ArrayList<View>();
		// for (Map.Entry<Integer, Double> entry : winnerPot.entrySet()) {
		//
		// GamePlayerView pView = playersList.get(entry.getKey());
		// winners.add(pView);
		// ChipView chipView = new ChipView(entry.getValue(), this);
		// View v = chipView.getView();
		// RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
		// 50, 50);
		// params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
		// v.setLayoutParams(params);
		//
		// potViews.add(v);
		// }
		// mRoot.addView(potViews.get(0));
		//
		// WinAnimation anim = new WinAnimation(winners, potViews.get(0), null,
		// potViews, mRoot);
		// anim.run();
	}

	/*
	 * SETTERS AND GETTERS
	 */

	public void setOwnPlayerLayout(int i) {
		this.ownPlayerLayout = this.playersList.get(i);
	}

	public ProgressDialog getProgressDialog() {
		return progressDialog;
	}

	public Button getCallBtn() {
		return callBtn;
	}

	public Button getFoldBtn() {
		return foldBtn;
	}

	public Button getCheckBtn() {
		return checkBtn;
	}

	public Button getRaiseBtn() {
		return raiseBtn;
	}

	public ProgressBar getTimeProgressbar() {
		return timeProgressbar;
	}

	public void setTimeProgressbar(ProgressBar timeProgressbar) {
		this.timeProgressbar = timeProgressbar;
	}

	public SeekBar getSeekBar() {
		return seekBar;
	}

	public void setSeekBar(SeekBar seekBar) {
		this.seekBar = seekBar;
	}

	public AlertDialog getmRaiseDialog() {
		return mRaiseDialog;
	}

	public void setmRaiseDialog(AlertDialog mRaiseDialog) {
		this.mRaiseDialog = mRaiseDialog;
	}

	public GameController getController() {
		return controller;
	}

	public void setController(GameController controller) {
		this.controller = controller;
	}

	public Button[] getButtons() {
		return buttons == null ? null : (Button[]) buttons.clone();
	}

	public void setButtons(Button[] buttons) {
		this.buttons = buttons;
	}

	public GamePlayerView getPlayerById(int id) {
		return playersList.get(id);
	}

	public TextView getTableCurrentMoney() {
		return tableCurrentMoney;
	}

	public void setTableCurrentMoney(TextView tableCurrentMoney) {
		this.tableCurrentMoney = tableCurrentMoney;
	}

	public double getUserCurrentMoney() {
		return userCurrentMoney;
	}

	public void setUserCurrentMoney(double userCurrentMoney) {
		this.userCurrentMoney = userCurrentMoney;
	}

}
