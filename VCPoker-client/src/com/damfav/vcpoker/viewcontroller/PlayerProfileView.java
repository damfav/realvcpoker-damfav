 /* PlayerProfileView.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.viewcontroller;

import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.damfav.vcpoker.model.Card;
import com.damfav.vcpoker.model.Profile;
import com.damfav.vcpoker_client.R;

/**
 * This class defines the player profile view
 */
public class PlayerProfileView extends RelativeLayout {

	/**
	 * The profile layout where the information would go
	 */
	private RelativeLayout profileLayout;

	/**
	 * The typeface to use in the texviews
	 */
	private Typeface typeFace;

	/**
	 * Default contructor
	 * 
	 * @param context
	 */
	public PlayerProfileView(Context context) {
		super(context);
		initLayout();
	}

	/**
	 * This constructor creates the playerProfileView
	 * 
	 * @param context
	 *            The context where application will go on
	 * @param attrs
	 *            The view Attributes
	 */
	public PlayerProfileView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initLayout();
	}

	/**
	 * This method initialize the player view
	 */
	private void initLayout() {
		// create a new relativelayout inflating the xml
		profileLayout = (RelativeLayout) View.inflate(getContext(),
				R.layout.activity_game_rl_userinfo, null);

		// put a new layoutparams in the playerprofile view
		profileLayout.setLayoutParams(new LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT));

		// add the created layout in to this layout
		this.addView(profileLayout);

		// load the custom typeface
		typeFace = Typeface.createFromAsset(getResources().getAssets(),
				"font/mrsmonster.ttf");

		// setProfileinfo(Client.getInstance().getPlayerInfo());

	}

	/**
	 * This method change the information of the view with the Profile object
	 * 
	 * @param userProfile
	 *            profile to set
	 */
	@SuppressWarnings("unused")
	private void setProfileinfo(Profile userProfile) {

		TextView textViewUsername = (TextView) findViewById(R.id.userInfo_tv_Username);
		textViewUsername.setText(userProfile.getName());
		textViewUsername.setTypeface(typeFace);

		TextView textViewCurrentMoney = (TextView) findViewById(R.id.userInfo_tv_CurrentMoneyLabel);
		textViewCurrentMoney.setText("Current money: "
				+ 99);
		textViewCurrentMoney.setTypeface(typeFace);

		TextView textViewBiggestWin = (TextView) findViewById(R.id.userInfo_tv_BiggestWinLabel);
		textViewBiggestWin.setText("Biggest Win: "
				+ userProfile.getBiggestWin());
		textViewBiggestWin.setTypeface(typeFace);

		TextView textViewTotalWins = (TextView) findViewById(R.id.userInfo_tv_totalsWinLabel);
		textViewTotalWins.setText("Total wins: " + userProfile.getTotalWins());
		textViewTotalWins.setTypeface(typeFace);

		TextView textViewBestHand = (TextView) findViewById(R.id.userInfo_tv_BesthandLabel);
		textViewBestHand.setTypeface(typeFace);

		List<Card> list = userProfile.getBestHand();
		LinearLayout bestHandLinearLayout = (LinearLayout) findViewById(R.id.profileActivityCardListLayoutID);
		for (int i = 0; i < list.size(); i++) {
			Card card = list.get(i);
			String cardName = Utilities.getCardName(card);
			Drawable d = getResources().getDrawable(
					getResources().getIdentifier(cardName, "drawable",
							getContext().getPackageName()));
			ImageView currentCardImageView = ((ImageView) bestHandLinearLayout
					.getChildAt(i));
			int heOld = currentCardImageView.getLayoutParams().height;
			currentCardImageView.setImageDrawable(d);
			currentCardImageView.getLayoutParams().height = heOld;

		}
	}

}
