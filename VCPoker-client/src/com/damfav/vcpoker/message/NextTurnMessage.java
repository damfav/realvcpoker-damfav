/* NextTurnMessage.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.message;

/**
 * Model message for the next turn with all the possible buttons and the last
 * bet
 */
public class NextTurnMessage extends AbstractMessage {

	/**
	 * THe possible buttons can do the next player
	 */
	private boolean[] possibleButtons;

	/**
	 * The last bet
	 */
	private double lastBet;

	public NextTurnMessage(boolean[] possibleButtons, double lastBet) {
		super();
		this.possibleButtons = possibleButtons;
		this.lastBet = lastBet;
	}

	// GETTERS & SETTERS
	@Override
	public int getCode() {
		return MessageFactory.MSG_NEXTTURN;
	}

	public boolean[] getPossibleButtons() {
		return possibleButtons == null ? null : (boolean[]) possibleButtons.clone();
	}

	public void setPossibleButtons(boolean[] possibleButtons) {
		this.possibleButtons = possibleButtons;
	}

	public double getLastBet() {
		return lastBet;
	}

	public void setLastBet(double lastBet) {
		this.lastBet = lastBet;
	}

}
