/* SoundManager.java 1.0
 *
 *	 Copyright 2014	Awais Iqbal Begum <awais.iqbal2@gmail.com>
 *	 				Francesc Bautista Saiz <fbautistasaiz@gmail.com>
 *	 				Victor Ruiz Ramos <victor.rura@gmail.com>
 *
 *	 The MIT License (MIT)
 *
 *	 Permission is hereby granted, free of charge, to any person obtaining a copy
 *	 of this software and associated documentation files (the \"Software\"), to deal
 *	 in the Software without restriction, including without limitation the rights
 *	 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	 copies of the Software, and to permit persons to whom the Software is
 *	 furnished to do so, subject to the following conditions:
 *
 *	 The above copyright notice and this permission notice shall be included in
 *	 all copies or substantial portions of the Software.
 *
 *	 THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	 THE SOFTWARE.
 */

package com.damfav.vcpoker.view.sound;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

import com.damfav.vcpoker.viewcontroller.Constants;
import com.damfav.vcpoker_client.R;

/**
 * SoundManager plays diferents sound telling the sound ID, using the class
 * {@link Constants}
 */
public class SoundManager {
	/**
	 * Pool of sounds
	 */
	private SoundPool mSoundPool;

	/**
	 * Map of soundPools
	 */
	private HashMap<Integer, Integer> mSoundPoolMap;

	/**
	 * AudioManager to manage audio settings
	 */
	private AudioManager mAudioManager;

	/**
	 * Sound context
	 */
	private Context mContext;

	public SoundManager(Context ctx) {
		this.mContext = ctx;
		initSounds(mContext);
		loadSounds();
	}

	/**
	 * Initializes the storage for the sounds theContext The Application context
	 */
	@SuppressLint("UseSparseArrays")
	public void initSounds(Context theContext) {
		mContext = theContext;
		mSoundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);
		mSoundPoolMap = new HashMap<Integer, Integer>();
		mAudioManager = (AudioManager) mContext
				.getSystemService(Context.AUDIO_SERVICE);
	}

	/**
	 * Add a new Sound to the SoundPool
	 * 
	 * 
	 * @param Index
	 *            - The Sound Index for Retrieval
	 * @param SoundID
	 *            - The Android ID for the Sound asset.
	 */
	public void addSound(int Index, int SoundID) {
		mSoundPoolMap.put(Index, mSoundPool.load(mContext, SoundID, 1));
	}

	/**
	 * Loads the various sound assets Currently hardcoded but could easily be
	 * changed to be flexible.
	 */
	public void loadSounds() {
		mSoundPoolMap.put(0, mSoundPool.load(mContext, R.raw.bet, 1));
		mSoundPoolMap.put(1, mSoundPool.load(mContext, R.raw.all_in, 1));
		mSoundPoolMap.put(2, mSoundPool.load(mContext, R.raw.check, 1));
		mSoundPoolMap.put(3, mSoundPool.load(mContext, R.raw.fold, 1));
		mSoundPoolMap.put(4, mSoundPool.load(mContext, R.raw.winning, 1));
		mSoundPoolMap.put(5, mSoundPool.load(mContext, R.raw.dealing, 1));
		mSoundPoolMap.put(6, mSoundPool.load(mContext, R.raw.turn_card, 1));

	}

	/**
	 * 
	 * @param index
	 *            - The Index of the Sound to be played
	 * @param speed
	 *            - The Speed to play not, not currently used but included for
	 *            compatibility
	 */
	public void playSound(int index) {
		float streamVolume = mAudioManager
				.getStreamVolume(AudioManager.STREAM_MUSIC);
		streamVolume = streamVolume
				/ mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		mSoundPool.play(mSoundPoolMap.get(index), streamVolume, streamVolume,
				1, 0, 1);
	}

	/**
	 * Stop a Sound
	 * 
	 * @param index
	 *            - index of the sound to be stopped
	 * 
	 */
	public void stopSound(int index) {
		mSoundPool.stop(mSoundPoolMap.get(index));
	}

	/**
	 * Releases and clears sound managing objects.
	 */
	public void cleanup() {
		mSoundPool.release();
		mSoundPool = null;
		mSoundPoolMap.clear();
		mAudioManager.unloadSoundEffects();

	}
}
