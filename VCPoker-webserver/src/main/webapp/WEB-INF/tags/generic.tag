<%@ tag language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <jsp:include page="/WEB-INF/views/layouts/tpl_header.jsp" />

</head>
<body>
	<div id="pageheader">
		<jsp:include page="/WEB-INF/views/layouts/header.jsp" />
	</div>
	<div id="body">
		<jsp:doBody />
	</div>
	<div id="pagefooter">

		<jsp:include page="/WEB-INF/views/layouts/footer.jsp" />

	</div>
</body>
</html>