<%@page import="com.damfav.vpoker.views.JspHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
     pageEncoding="UTF-8"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
    
	<nav class="navbar navbar-default mynavbar" role="navigation">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Brand</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="main-nav">
          <ul class="nav navbar-nav">
            <li><a href="/">Home</a></li>
            <li><a href="/statistics">Statistics</a></li>
          </ul>
		  <jsp:include page="login-main.jsp" />
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
    
	<t:modal id="loginModal">
		<jsp:attribute name="title">Login</jsp:attribute>
		<jsp:attribute name="body">
		</jsp:attribute>
		<jsp:attribute name="footer">
			<button type="button" class="loginBtnModal btn btn-success">Send</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</jsp:attribute>
	</t:modal>
	
	<t:modal id="signUpModal">
		<jsp:attribute name="title">Sign Up</jsp:attribute>
		<jsp:attribute name="body">
		</jsp:attribute>
		<jsp:attribute name="footer">
			<button type="button" class="signUpBtnModal btn btn-success">Send</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</jsp:attribute>
	</t:modal>

<% JspHelper.addScript("/header.js"); %>
