 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<ul id="loginSectionMainNav" class="nav navbar-nav navbar-right">
	<li><c:choose>
			<c:when test="${sessionScope.login != null}">
          			Welcome ${sessionScope.login}
          			<button type="button" class="btn btn-primary btn-lg logoutBtn">LogOut</button>
			</c:when>
			<c:otherwise>
				<button type="button" class="btn btn-primary btn-lg loginBtn" >LogIn</button>
				<button type="button" class="btn btn-primary btn-lg signUpBtn">SignUp</button>
			</c:otherwise>
		</c:choose></li>
</ul>