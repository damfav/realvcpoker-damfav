<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<form:form method="POST" action="/user/signUp" role="form" commandName="signUp">
  <div class="form-group">
    <form:label path="email">Email address</form:label>
    <form:input path="email" type="email" class="form-control" placeholder="Enter email" />
    <form:errors path="email" cssClass="error"/>
  </div>
  <div class="form-group">
    <form:label path="password">Password</form:label>
    <form:input path="password" type="password" class="form-control" placeholder="Enter password" />
    <form:errors path="password" cssClass="error"/>
  </div>
  <div class="form-group">
    <form:label path="confirmPassword">Confirm Password</form:label>
    <form:input path="confirmPassword" type="password" class="form-control" placeholder="Enter password again" />
    <form:errors path="confirmPassword" cssClass="error"/>
  </div>
</form:form>