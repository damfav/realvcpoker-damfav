<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<form:form method="POST" action="/user/login" role="form" commandName="user">
  <div class="form-group">
    <form:label path="email">Email address</form:label>
    <form:input path="email" type="email" class="form-control" placeholder="Enter email" />
    <form:errors path="email" cssClass="error"/>
  </div>
  <div class="form-group">
    <form:label path="password">Password</form:label>
    <form:input path="password" type="password" class="form-control" placeholder="Enter password" />
    <form:errors path="password" cssClass="error"/>
  </div>
</form:form>