
function MStatistics(){
	
}

MStatistics.prototype = {
		
	initEventListeners : function () {
	
	},
	
	initDataTables: function () {
	
		$('#dataTable').dataTable({
			"bProcessing" : true,
			"sAjaxSource" : '/statistics/jsonFeed',
			"bServerSide": true,
	        "aoColumns": [
	                      { "sTitle": "Cards" },
	                      { "sTitle": "Action" },
	                  ]
		});
	
	}
};

var mStatistics = new MStatistics();

$(document).ready(function() {
	mStatistics.initDataTables();
	mStatistics.initEventListeners();
});
