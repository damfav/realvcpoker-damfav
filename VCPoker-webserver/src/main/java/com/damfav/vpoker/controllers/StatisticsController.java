package com.damfav.vpoker.controllers;

import java.io.IOException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.damfav.vpoker.models.domain.PokerEvent;
import com.damfav.vpoker.models.repositories.PokerEventRepository;
import com.damfav.vpoker.views.DataTable;
import com.google.gson.Gson;

@Controller
public class StatisticsController extends MyController{
	
	@RequestMapping(value = "/statistics/jsonFeed", method = RequestMethod.GET)
	public void jsonFeed(HttpServletResponse resp, HttpServletRequest req){
		PokerEventRepository pkRepo = new  PokerEventRepository();
		List<PokerEvent> pkEvents = pkRepo.findAll();
		Map<String, Object> map = new HashMap<String, Object>();
		String sEcho = req.getParameter("sEcho");
		map.put("sEcho", sEcho == null ? 1 : Integer.parseInt(sEcho));
		map.put("iTotalRecords", pkEvents.size());
		map.put("iTotalDisplayRecords", 10);
		map.put("aaData", DataTable.transformStatisticsJson(pkEvents));
		String json = new Gson().toJson(map);
		try {
			resp.getWriter().println(json);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/statistics/trackEvent", method = RequestMethod.POST)
	public void trackEvent(PokerEvent ev){
		PokerEventRepository repo = new PokerEventRepository();
		PokerEvent exist = repo.findByExample(ev);
		if (exist != null){
			exist.incCount();
			repo.save(exist);
		}else{
			repo.save(ev);
		}
	}
	
	@RequestMapping(value = "/statistics/trackEventJson", method = RequestMethod.POST)
	public void trackEventJson(HttpServletRequest req, HttpServletResponse resp){

		Gson gson = new Gson();
		PokerEvent ev = gson.fromJson(req.getParameter("ev"), PokerEvent.class);
		 
		PokerEventRepository repo = new PokerEventRepository();
		PokerEvent exist = repo.findByExample(ev);
		if (exist != null){
			exist.incCount();
			repo.save(exist);
		}else{
			repo.save(ev);
		}
	}

}
