package com.damfav.vpoker.controllers;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.RescaleOp;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.imgscalr.Scalr;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.damfav.vpoker.models.constraints.NotEmailExistsConstraintValidator;
import com.damfav.vpoker.models.domain.Profile;
import com.damfav.vpoker.models.domain.User;
import com.damfav.vpoker.models.forms.SignUpForm;
import com.damfav.vpoker.models.repositories.ProfileRepository;
import com.damfav.vpoker.models.repositories.UserRepository;
import com.damfav.vpoker.models.utils.MD5Encrypter;



@Controller
public class UserController extends MyController {

	private UserRepository userRep;
	
	private ProfileRepository profRep;
	
	public UserController(){
		super();
		userRep = new UserRepository();
		profRep = new ProfileRepository();
	}
	
	@RequestMapping(value = "/user/login", method = RequestMethod.POST)
	public String login(User user,
			HttpServletRequest request, HttpServletResponse response) {
		String viewToReturn = null;
		if (user.getEmail() == null || user.getEmail().isEmpty()){
			request.getSession().setAttribute("login", request.getRemoteAddr());
			user.setEmail(request.getRemoteAddr());
			if (!userRep.exists(user)){
				userRep.save(user);
				profRep.createBasicProfile(user);
			}
			viewToReturn = "layouts/login-main";
		}
		else if (!userRep.correctLogin(user)) {
			response.setStatus(257);
			viewToReturn = view("_login-form");
		} else {
			request.getSession().setAttribute("login", user.getEmail());
			viewToReturn = "layouts/login-main";
		}

		return viewToReturn;
	}

	@RequestMapping(value = "/user/logout", method = RequestMethod.POST)
	public String logout(HttpServletRequest request) {
		request.getSession().removeAttribute("login");
		return "layouts/login-main";
	}

	@RequestMapping(value = "/user/getLoginForm", method = RequestMethod.GET)
	public String getLoginForm(Model model) {
		model.addAttribute("user", new User());
		return view("_login-form");
	}
	
	@RequestMapping(value = "/user/getSignUpForm", method = RequestMethod.GET)
	public String getSignUpForm(Model model) {
		model.addAttribute("signUp", new SignUpForm());
		return view("_signup-form");
	}

	@RequestMapping(value = "/user/signUp", method = RequestMethod.POST)
	public String signUp(HttpServletResponse resp, @Valid SignUpForm signUp, BindingResult errors, Model model) {
		model.addAttribute("signUp", signUp);
		NotEmailExistsConstraintValidator valid = new NotEmailExistsConstraintValidator();
		boolean isValid = valid.isValid(signUp.getEmail(), null);
		if (!isValid){
			errors.addError(new ObjectError("email", "email exists"));
		}
		
		if (errors.hasErrors()){
			resp.setStatus(257);
		}else{
			User u = new User();
			u.setEmail(signUp.getEmail());
			u.setPassword( MD5Encrypter.encryptPassword(signUp.getPassword()) );
			
			userRep.save(u);
			profRep.createBasicProfile(u);
			

		}	
		
		return view("_signup-form");
		
	}
	
	@RequestMapping(value="/user/getProfile",method = RequestMethod.POST)
	public void getProfile(HttpServletRequest req, HttpServletResponse resp){
		String email = req.getParameter("email");
	
		if (email == null || email.isEmpty()){
			email = req.getRemoteAddr();
		}
		
		Profile profile = profRep.findByEmail(email);

		try {
			if (profile != null){
				OutputStream out = resp.getOutputStream();
				String jsonProfile = profile.toJson() + "\n";
				out.write(jsonProfile.getBytes());
				String avatar = profile.getAvatarUrl();
				File f = null;
				if (avatar != null){
					f = new File(avatar);
				}
				if (f != null && f.exists()){
					byte[] bytes = Files.readAllBytes(f.toPath());
					out.write(bytes);
				}
				out.flush();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		
		
	}
	
	@RequestMapping(value="/user/updateProfile",method = RequestMethod.POST)
	public void updateProfile(Profile updateProf, HttpServletRequest req, HttpServletResponse resp){
		String email = req.getParameter("user_email");
		System.out.println(email);
		Profile prof = profRep.findByEmail(email);
		
		Map<String, String[]> map = req.getParameterMap();

		
		prof.setAttributes(map);
		
		profRep.save(prof);
	}
	
	@RequestMapping(value="/user/updateAvatar",method = RequestMethod.POST)
	public void updateAvatar(@RequestParam("avatar") MultipartFile file, HttpServletRequest req, HttpServletResponse resp){
		String email = req.getParameter("user_email");
		System.out.println(email);
		Profile prof = profRep.findByEmail(email);
		System.out.println(file.getName());
        if (!file.isEmpty()) {
            try {
  
            	BufferedImage img = ImageIO.read(file.getInputStream());
            	
            	BufferedImage scaled = Scalr.resize(img, 40, 60, Scalr.OP_ANTIALIAS);
            	String oriName = file.getOriginalFilename();
            	String extension = oriName.substring(oriName.lastIndexOf(".") + 1);
            	
            	String root = System.getenv("OPENSHIFT_DATA_DIR");
                // Creating the directory to store file
            	String avatarDir = root + "images";
            	System.out.println(avatarDir);
            	File dir = new File(avatarDir);
                if (!dir.exists()){
                    dir.mkdirs();
                }
        		String filename = avatarDir + "/" + email + "." + extension;

                // Create the file on server
                File serverFile = new File(filename);
                BufferedOutputStream stream = new BufferedOutputStream(
                        new FileOutputStream(serverFile));
                ImageIO.write(scaled, extension, serverFile);
                stream.close();
 
        		prof.setAvatarUrl(filename);
        		profRep.save(prof);
 
            } catch (Exception e) {
            	System.out.println("Exception");
            	resp.setStatus(257);
            }
        } else {
        	System.out.println("FILE EMPTY");
        	resp.setStatus(257);
        }

	}
}
