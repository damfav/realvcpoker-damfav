package com.damfav.vpoker.controllers;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.damfav.vpoker.models.domain.User;
import com.damfav.vpoker.models.repositories.UserRepository;


/**
 * Handles requests for the application home page.
 */
@Controller
public class SiteController extends MyController{
	

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		model.addAttribute("user", new User());
		return view("home");
	}
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/statistics", method = RequestMethod.GET)
	public String statistics(Locale locale, Model model) {
		model.addAttribute("user", new User());
		return view("statistics");
	}

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public void statictics(HttpServletResponse out, Model model) {
		UserRepository userRep = new UserRepository();
		try {
			out.getWriter().println(userRep.findAll().size());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	
}
