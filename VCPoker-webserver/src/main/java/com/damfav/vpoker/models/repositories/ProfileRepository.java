package com.damfav.vpoker.models.repositories;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.damfav.vpoker.models.domain.Profile;
import com.damfav.vpoker.models.domain.User;

public class ProfileRepository extends AbstractRepository<Profile> {

	public ProfileRepository() {
		super(Profile.class);
		// TODO Auto-generated constructor stub
	}
	
	public Profile findByEmail(String email){
		Criteria c = new Criteria();
		c.and("user_email").is(email);
		
		return findOne(new Query(c));
	}

	public void createBasicProfile(User u){
		Profile p = new Profile();
		p.setUser_email(u.getEmail());
		p.setUsername("User");
		p.setBestHand("-");
		p.setTotalMoney("1000g 0r");
		
		this.save(p);
	}
}
