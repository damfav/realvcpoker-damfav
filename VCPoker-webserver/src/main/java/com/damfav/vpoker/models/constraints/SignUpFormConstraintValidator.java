package com.damfav.vpoker.models.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.damfav.vpoker.models.forms.SignUpForm;
import com.damfav.vpoker.models.repositories.UserRepository;

public class SignUpFormConstraintValidator implements ConstraintValidator<ValidSignUpForm, SignUpForm> {


	@Override
	public boolean isValid(SignUpForm target, ConstraintValidatorContext context) {
		return target.getPassword().equals(target.getConfirmPassword());
	}


	@Override
	public void initialize(ValidSignUpForm arg0) {
		// TODO Auto-generated method stub
		
	}


}