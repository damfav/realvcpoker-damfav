package com.damfav.vpoker.models.repositories;

import java.net.UnknownHostException;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.repository.MongoRepository;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import com.damfav.vpoker.models.domain.AbstractDocument;
import com.damfav.vpoker.models.utils.StatusCodes;
import com.mongodb.Mongo;
import com.mongodb.MongoException;


public abstract class AbstractRepository<T> implements MongoRepository<T, ObjectId> {
	
	protected MongoTemplate mongoTemplate;
	
	private static UserCredentials uCredentials = new UserCredentials("admin", "IpNeJeEqCRJE");
	
	private static String HOST = "127.2.89.131";
	
//	private static String HOST = "127.0.0.1";
	
	private Class<T> cls;
	
	public AbstractRepository(Class<T> cls){
		this.cls = cls;
		try {
			Mongo mongo = new Mongo(HOST, 27017);
//			mongoTemplate = new MongoTemplate(mongo, "damfav");
			mongoTemplate = new MongoTemplate(mongo, "damfav",uCredentials);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MongoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public Page<T> findAll(Pageable arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(ObjectId arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(T arg0) {
		// TODO Auto-generated method stub
		if (arg0 instanceof AbstractDocument){
			AbstractDocument abs = (AbstractDocument) arg0;
			abs.setStatus(StatusCodes.DELETED);
			mongoTemplate.save(arg0);
		}else{
			mongoTemplate.remove(arg0);
		}
	}

	@Override
	public void delete(Iterable<? extends T> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
	}
	
	
	public boolean exists(Query q){
		return mongoTemplate.exists(q, cls);
	}
	
	public T findOne(Query q){
		return mongoTemplate.findOne(q, cls);
	}

	@Override
	public <S extends T> S save(S arg0) {
		// TODO Auto-generated method stub
		boolean success = false;
		try{
			mongoTemplate.save(arg0);
			success = true;
		}catch (Exception e){
			
		}
		return success ? arg0 : null;
	}

	@Override
	public <S extends T> List<S> save(Iterable<S> entites) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<T> findAll(Query q){
		return mongoTemplate.find(q, cls);
	}

	@Override
	public boolean exists(ObjectId arg0) {
		// TODO Auto-generated method stub
		return exists( query(where("id").is(arg0)) );
	}

	@Override
	public Iterable<T> findAll(Iterable<ObjectId> arg0) {
		// TODO Auto-generated method stub
		Query q = new Query();
		Criteria criteria = new Criteria();
		criteria.in(arg0);
		q.addCriteria(criteria);
		return mongoTemplate.find(q, cls);
	}

	@Override
	public T findOne(ObjectId arg0) {
		// TODO Auto-generated method stub
		return mongoTemplate.findById(arg0, cls);
	}

	@Override
	public List<T> findAll() {
		// TODO Auto-generated method stub
		return mongoTemplate.findAll(cls);
	}

	@Override
	public List<T> findAll(Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

}
