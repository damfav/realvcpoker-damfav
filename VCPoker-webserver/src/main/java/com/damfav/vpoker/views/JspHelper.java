package com.damfav.vpoker.views;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.JspWriter;

public class JspHelper {
	private static List<String> scripts;
	private static List<String> css;
	
	public static void begin(){
		scripts = new ArrayList<String>();
		css = new ArrayList<String>();
	}
	
	public static void addScript(String src){
		scripts.add(src);
	}
	
	public static void addCss(String src){
		css.add(src);
	}
	
	public static void createScriptTags(JspWriter out){
		for (String scriptSrc : scripts){
			try {
				out.println("<script src='/resources/js" + scriptSrc + "'></script>");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void createCssTags(JspWriter out){
		for (String cssSrc : css){
			try {
				out.println("<link href='/resources/css" + cssSrc + "' rel='stylesheet' />");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
